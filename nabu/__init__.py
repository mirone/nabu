__version__ = "2020.5.1-dev"
__nabu_modules__ = [
    "app",
    "cuda",
    "distributed",
    "io",
    "misc",
    "opencl",
    "preproc",
    "reconstruction",
    "resources",
    "thirdparty",
]
version = __version__
