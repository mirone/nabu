from ..utils import get_octave_struct


class CtfParameters(dict):
    def __init__(self, octave_struct_filename = None):

        if not isinstance( octave_struct_filename, dict): 
            mydict = get_octave_struct(octave_struct_filename)
        else:
            mydict = octave_struct_filename

        for k in mydict:
            if isinstance(mydict[k], dict):
                mydict[k] = CtfParameters( mydict[k]["value"] )
        
        super(CtfParameters, self).__init__(  **mydict )
        
        self.__dict__ = self



        

# class __CtfParameters(dict):
    
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#     # this variables were present in holotomoslave
#     # and concern probably certain prepocessings which are
#     # already present in Nabu as module
#     # Therefore we set them here to default values
#     # which disactivate such generic preprocessing
#     # We will  keep, in the class, the CTF specific part
#     #           ||
#     radio_only   = False
#     do_flatfield = True
#     correct_flat = False
#     correct_ccd         = []
#     correct_distorsion  = []
#     image_threshold     = 0.0
#     remove_spikes       = False
#     save_radiographs_slice = False
#     undo_log               = False
#     save_projection        = False
#     save_absorption        = False
#     show_projections       = False
#     # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
#     def set_dependencies(self):

#         if self.z1v is None:
#             self.z1v = self.z1

#         if self.z1h is None:
#             self.z1h = self.z1
            
#         if self.check_z1v is None:
#             self.check_z1v = 0

#         if len(self.numbers):
#             self.nb = len(self.numbers)
#         else:
#             self.nb = np.max([len(self.z2), len(self.z1h), len(self.z1v)])


#         if self.axisfilesduringscan is None:
#             self.logger.warning("axisfilesduringscan left to empty. The original holotomo_slave was retrieving it, in this case,"
#                                 " from a global variable named FT_AXISFILESDURINGSCAN which is not available in the actual version. Se it to None to remove this warning")            
#         # from prior estimate (absorption) the offset is imposed
#         # we should not modify it in order not to bias the RE
#         if self.preserve_offset is None:
#             self.preserve_offset = ( self.delta_beta != 0 )

#         if self.approach in [4,7,8]:
#             if self.nloop_mixed is None:
#                 self.nloop_mixed = 3
                
#         if self.approach == 5:
#             if self.simple_approach is None:
#                 self.simple_approach = False

#             if self.single_index is None:
#                 self.single_index = False
                
#         if self.approach == 7:
#             if self.simple_approach7 is None:
#                 self.simple_approach7 = -1
                
#     def __init__(self,
#             numbers = [],  #  when trial grid are explored, if given, this selects only certain points of the exploration grid
#             vers    = "", # dont know yet
#             cylinder = False,
#             ext_lowfreq = 0;
#             free_edge   = 0,
#             constrain   = 0,
#             recursive   = 0,
#             pad_images  = 1,
#             pad_method  = "extend",
#             extend_avg  = 20,  #  image is padded with average of extend_avg last values
#             approach    = 4,
#             modifieddelta = 0,
#             lim1  = 1.0e-8,
#             lim2  = "gaussian_filter",
#             var_cut = 1,
#             post_filer = [],
#             delta_beta = 0,
#             test_par_polyfit_order = math.inf, # infinity
#             Lcurve  = 0,
#             preserve_offset = None,
#             calc_residu     = 0,
#             nloop_mixed = None,
#             simple_approach = None,
#             single_index = None,
#             simple_approach7 = None,
#             ssigma = 10,
#             strong_absorption = False,
#             subtr_filterd_mean = False,
#             centralpart        = 1,
#             magnification      = 1.0,
#             magnification_large = 0,
#             axisposition        = "global",
#             axisfilesduringscan = [],
#             shift_constant= 0,
#             shift_show= 1,
#             shift_show_contrast= 1,
#             shift_approach= 2, # findshift
#             shift_useprevious= 1, # for shift_approach 2 and 3 
#             shift_constrain= 0,
#             shift_interactive= 0,
#             shift_polyfit_order_h= 3,
#             shift_polyfit_order_v= 2,
#             shift_singleplot= 0,
#             shift_iterative= 0,
#             projections = [],
#             z1 = None,
#             z1v= None,
#             check_z1v= None,
#             z1h= None
#             numbers = [],  #  when trial grid are explored, if given, this selects only certain points of the exploration grid
#             vers    = "", # dont know yet
#             cylinder = False,
#             ext_lowfreq = 0;
#             free_edge   = 0,
#             constrain   = 0,
#             recursive   = 0,
#             pad_images  = 1,
#             pad_method  = "extend",
#             extend_avg  = 20,  #  image is padded with average of extend_avg last values
#             approach    = 4,
#             modifieddelta = 0,
#             lim1  = 1.0e-8,
#             lim2  = "gaussian_filter",
#             var_cut = 1,
#             post_filer = [],
#             delta_beta = 0,
#             test_par_polyfit_order = math.inf, # infinity
#             Lcurve  = 0,
#             preserve_offset = None,
#             calc_residu     = 0,
#             nloop_mixed = None,
#             simple_approach = None,
#             single_index = None,
#             simple_approach7 = None,
#             ssigma = 10,
#             strong_absorption = False,
#             subtr_filterd_mean = False,
#             centralpart        = 1,
#             magnification      = 1.0,
#             magnification_large = 0,
#             axisposition        = "global",
#             axisfilesduringscan = [],
#             shift_constant= 0,
#             shift_show= 1,
#             shift_show_contrast= 1,
#             shift_approach= 2, # findshift
#             shift_useprevious= 1, # for shift_approach 2 and 3 
#             shift_constrain= 0,
#             shift_interactive= 0,
#             shift_polyfit_order_h= 3,
#             shift_polyfit_order_v= 2,
#             shift_singleplot= 0,
#             shift_iterative= 0,
#             projections = [],
#             z1 = None,
#             z1v= None,
#             check_z1v= None,
#             z1h= None
#     ):
        
#         passed_variables = locals()
#         del passed_variables["self"]
#         del passed_variables["__class__"]
        
#         super(CtfParameters, self).__init__(  **passed_variables )
        
#         self.__dict__ = self

#         self.set_dependencies()
