#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from math import pi
from bisect import bisect
from ..utils import generate_powers, PaddingMode
import math
import copy

from .ctf_parameters import CtfParameters


class PhaseRetrievalCTF(object):

    powers = generate_powers()
    implemented_approachs = [1]

    def __init__(
            self,
            parameters
    ):
        
        if not isinstance( parameters, CtfParameters) :
            
            message = ( "The parameters must be passed through a an object of the CtfParameters class."
            "What you passed is insead an object of the {} class.").format(   parameters.__class__ )
            raise ValueError(message)

        self.parameters = parameters

        if self.parameters.approach not in self.implemented_approaches:
            
            message = ( "so far the only implemented approaches are  {}. While you are asking for approach {}.").format(   self.implemented_approaches, self.parameters.approach )
            raise ValueError(message)
        
        self.compute_filter()

