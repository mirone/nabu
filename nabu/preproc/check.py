from h5py import *
from numpy import *
from nabu.preproc.ctf_parameters import CtfParameters
import fabio


def check(a,b, na, nb, zero = None):
    d=a-b
    if zero is not None:
        d[zero]=0
    print( na, nb,  abs(d).max(), abs(a).mean() , abs(b).mean()  ,   unravel_index( argmax(  abs(d)), d.shape )   )
    
im_orig_p = File("im_orig.h5","r")["data"][()]
im_orig_o = CtfParameters("images.h5").im
check( im_orig_p, im_orig_o, "im_orig_p","im_orig_o" )



a = File("im_mdark.h5","r")["data"][()]
b = CtfParameters("images.h5").im_dark
check( a, b, "im_mdark_p","im_mdark_o" )


ff_orig_p = File("flat_a.h5","r")["data"][()]
ff_orig_o = CtfParameters("ff_o.h5").ff

check( ff_orig_p, ff_orig_o, "ff_orig_p","ff_orig_o" )

a = File("cor1.h5","r")["data"][()]
b = CtfParameters("cors_o.h5").cor1
check( a, b, "cor1_p","cor1_o" )


a = File("cor2.h5","r")["data"][()]
b = CtfParameters("cors_o.h5").cor2
check( a, b, "cor2_p","cor2_o" )

a = File("cor1_interp.h5","r")["data"][()]
b = CtfParameters("cors_inter_o.h5").cor1
check( a, b, "cor1_interp_p","cor1_inter_o" )


a = File("cor2_interp.h5","r")["data"][()]
b = CtfParameters("cors_inter_o.h5").cor2
check( a, b, "cor2_interp_p","cor2_inter_o" )

a = File("flat_c.h5","r")["data"][()]
b = CtfParameters("flat_interp.h5").f_interp
check( a, b, "f_interp_p","f_inter_o" )

a = File("im_flat.h5","r")["data"][()]
b = CtfParameters("risultati_flat.h5").im_flat
check( a, b, "im_flat","risultati_flat" )

a = File("im_spikes.h5","r")["data"][()]
b = CtfParameters("im_spikes_o.h5").im
check( a, b, "im_spikes","im_spikes_o" )

a = File("im_spikes_before.h5","r")["data"][()]
b = CtfParameters("im_spikes_before_o.h5").im
check( a, b, "im_spikes_before","im_spikes_before_o" )

a = File("padded.h5","r")["data"][()]
b = CtfParameters("padded_o.h5").im
check( a, b, "padded","padded_o" )



a = File("padded_translated.h5","r")["data"][()]
b = CtfParameters("padded_translated_o.h5").im
check( a, b, "padded_translated","padded_translated_o" )



a = File("im_interp.h5","r")["data"][()]
b = CtfParameters("dopo_o.h5").im
check( a, b, "im_interp","im_interp_o" )


a = File("ssq_p.h5","r")["data"][()]
b = CtfParameters("ssq_r.h5").im
check( a, b, "ssq","ssq_o" )



a = File("im_mean.h5","r")["data"][()]
b = CtfParameters("im_mean_o.h5").data
check( a, b, "im_mean","im_mean_o" )

a = File("im_fft.h5","r")["data"][()].real
b = CtfParameters("im_fft_o.h5").data
check( a, b, "im_fft","im_fft_o"  , zero = [[2048],[2048]])



a = File("firec0.h5","r")["data"][()].real
b = CtfParameters("firec0_r.h5").im
check( a, b, "firec0","firec0_o" , zero = [[2048],[2048]])

a = File("firec0.h5","r")["data"][()].imag
b = CtfParameters("firec0_i.h5").im
check( a, b, "firec0","firec0_i" , zero = [[2048],[2048]])



a = File("pfiltered.h5","r")["data"][()].real
b = CtfParameters("post_pfiltered_r.h5").im
check( a, b, "pfiltered","pfiltered_o" , zero = [[2048],[2048]])

a = File("pfiltered.h5","r")["data"][()].imag
b = CtfParameters("post_pfiltered_i.h5").im
check( a, b, "pfiltered","pfiltered_i" , zero = [[2048],[2048]])


a = File("delta.h5","r")["data"][()].real
b = CtfParameters("delta_r.h5").im
check( a, b, "delta","delta_i" , zero = [[2048],[2048]])

File("diff.h5","w")["diff"] = a-b
File("diff.h5","r+")["a"] = a
File("diff.h5","r+")["b"] = b


a= fabio.open("phase0001.edf").data
b= fabio.open("o.edf").data
check( a, b, "prova","oct" )



a = File("presult.h5","r")["data"][()].real
b = CtfParameters("ifft_ph_o.h5").im
check( a, b, "presult","iff_ph_o" )
