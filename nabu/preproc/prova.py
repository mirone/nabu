from nabu.resources.logger import LoggerOrPrint
import numpy as np
import logging
import h5py
import math
from scipy.special import erfc
import collections

from nabu.utils import median2 as nabu_median_filter
from nabu.preproc.alignment import AlignmentBase
import  scipy.interpolate
import scipy.signal
import fabio

class GeoPars(object):
    def __init__(self, oct_params, logger = None, le = 10.0e-6):
        
        self.logger = LoggerOrPrint(logger)
        
        self.mf = int(oct_params.geo_params.mf)
        self.nf = int(oct_params.geo_params.nf)
        self.m  = int(oct_params.geo_params.m )
        self.n  = int(oct_params.geo_params.n ) 
        
        self.logger.info( f"Padding : original ({self.m}x{self.n}) ;  target ({self.mf}x{self.nf}) ")

        self.z1h = oct_params.geo_params.z1h 
        self.z2h = oct_params.geo_params.z2h 
        self.z1v = oct_params.geo_params.z1v 
        self.z2v = oct_params.geo_params.z2v 

        self.magnification = oct_params.geo_params. magnification
        self.magnification_large = oct_params.geo_params. magnification_large

        self.pixelsize_detector = oct_params.geo_params. pixelsize_detector

        if self.magnification :
            self.Mh = (self.z1h+self.z2h)/self.z1h
            self.Mv = (self.z1v+self.z2v)/self.z1v
        else:
            self.Mh = 1.0
            self.Mv = 1.0

        self.logger.info( f"Magnification : h ({self.Mh}) ;  v ({self.Mv}) ")
        
        self.pixel_size_detector = oct_params.geo_params. pixelsize_detector

        self.le = le
        self.lambd = oct_params.lambd
        
        maxM = max( self.Mh, self. Mv )
        
        self.pixelsize = self.pixelsize_detector / maxM  # we bring everything to highest magnification

        which_unit = np.sum(  np.array([   (self.pixelsize > small)  for small in [1.0e-6, 1.0e-7  ] ]).astype(np.int32)  )
        self.pixelsize_string = [  f"{self.pixelsize*1e9:.1f} nm",f"{self.pixelsize*1e6:.3f} um" ,f"{self.pixelsize*1e6:.1f} um"            ][which_unit]

        if self.magnification:
            self.logger.info(f"All images are resampled to smallest pixelsize: {self.pixelsize_string}")
        else:
            self.logger.info(f"Pixelsize images:  {self.pixelsize_string}")
                             

class  DetectionCorrector(object):
    def __init__(self, rh=None, dark = None, fs = None , refns = None, logger = None, remove_spikes = 0.0, geo_pars = None) :
        self.logger = LoggerOrPrint(logger)
        self.rh = rh
        self.geo_pars  = geo_pars
        if self.rh is not None:
            self.logger.info( f"RH : setted by argument. Its shape is {self.rh.shape}, it std is  {self.rh.std()}")

        self.dark  = dark
        self.fs    = np.array(fs)
        self.refns =  np.array( refns )

        self.remove_spikes = remove_spikes

        if self.fs.shape[0] != self.refns.shape[0]:
            message = """
            """

    def recut( self, im   ):
        return centered_cut(im , self.geo_pars.n, self.geo_pars.m  )

            
    def pad_interpolate( self, im, translation = None):
        pad_extra = 2 **( 1 + np.ceil(np.log2(abs(np.array(translation)) )).astype(np.int32))

        print(" pad_extra ")
        print( pad_extra)

        
        print( " DIMENSION ima   "  )
        print( self.geo_pars.nf, self.geo_pars.mf        ) 
        print( self.geo_pars.n , self.geo_pars.m        )


        origy, origx = im.shape
        rety = self.geo_pars.nf + pad_extra[0]
        retx = self.geo_pars.mf + pad_extra[1]

        xpad    = [0,0]
        xpad[0] =  math.ceil((retx-origx)/2)
        xpad[1] =  retx-origx-xpad[0]

        ypad    = [0,0]
        ypad[0] =  math.ceil((rety-origy)/2)
        ypad[1] =  rety-origy-ypad[0]
        
        y2 = origy-ypad[1]
        x2 = origx-xpad[1]


        print(f"  origy {origy} origx {origx}   rety {rety} retx {retx} xpad {xpad} ypad {ypad} x2 {x2} y2 {y2}   ")
        
        if(ypad[0]+1 > origy or xpad[0]+1 > origx or y2 < 1 or x2 < 1):
            raise ValueError("Too large padding for this reflect padding type");


        ## REFLECTION PADDING
        ## ** the slice class is a python builtin selection tool 
        def inverted(s):
            """ s is a slice object defined by :  start,  stop, end 
            """
            nstop = s.start if s.start is None else s.start-s.step
            nstart = s.stop if s.stop  is None else s.stop -s.step
            return slice( nstart, nstop, -s.step)

        slice_left   = slice(     None  , xpad[0] , 1 )
        slice_right  = slice(   -xpad[1], None    , 1 )
        slice_bottom = slice(     None  , ypad[0] , 1 )
        slice_top    = slice(   -ypad[1], None    , 1 )

        central_y    = slice( ypad[0],-ypad[1],1   )
        central_x    = slice( xpad[0],-xpad[1],1   )


        retval = np.zeros(  [rety, retx]   , dtype = im.dtype )
        
        retval[ central_y, central_x ]  = im
        
        retval[ central_y     , slice_left     ] =  im[:,1:-1] [:, inverted(slice_left ) ]  
        #                                              ^^^ this shift-by-one trick because Peter does a mirror center over the border pixel, not on pixel border
        # and so on
        retval[ central_y     , slice_right    ] =  im[:,1:-1]  [ : , inverted(slice_right) ]
        
        retval[ slice_bottom  , central_x      ] =  im[1:-1, :  ]  [ inverted(slice_bottom ) , : ]
        
        retval[ slice_top     , central_x      ] =  im[1:-1, :  ]  [ inverted(slice_top    ) , : ]
        
        retval[ slice_bottom, slice_left   ] =  im[1:-1,1:-1][inverted(slice_bottom ), inverted(slice_left ) ]
        retval[ slice_bottom, slice_right  ] =  im[1:-1,1:-1][inverted(slice_bottom ), inverted(slice_right ) ]
        retval[ slice_top   , slice_left   ] =  im[1:-1,1:-1][inverted(slice_top    ), inverted(slice_left ) ]
        retval[ slice_top   , slice_right  ] =  im[1:-1,1:-1][inverted(slice_top    ), inverted(slice_right ) ]
                                                              

        h5py.File("padded.h5","w")["data"]=retval
        

        
        
        ## TRANSLATION X
        shift_x = translation[1]
        freqs = np.fft.fftfreq(retval.shape[1])
        shifts = np.exp( -2.0j*np.pi*freqs*shift_x)        
        retval = np.fft.ifft( np.fft.fft(retval, axis = 1)*shifts , axis = 1 ).real
        
        ## TRANSLATION X
        shift_y = translation[0]
        freqs = np.fft.fftfreq(retval.shape[0])
        shifts = np.exp( -2.0j*np.pi*freqs*shift_y)        
        retval = np.fft.ifft( np.fft.fft(retval, axis = 0)*shifts[:,None] , axis = 0 ).real

        print( f"shifts    {shift_y, shift_x} ")
        h5py.File("padded_translated.h5","w")["data"]=retval




        retval = centered_cut( retval, self.geo_pars.nf, self.geo_pars.mf     ) 


        return retval

        
    def apply_corrections(self,  im,  nradio , nsize = 100, correct_flat = 1):        
        h5py.File("im_orig.h5","w")["data"]=im
        if self.dark is not None:
            im = im - self.dark
            
        h5py.File("im_mdark.h5","w")["data"]=im


        if self.fs is not None:
            f_flat = np.interp( nradio, self.refns, np.arange(len(self.refns)).astype(np.float32)   )

            i_flat_0 = int(f_flat)
            f_flat   = f_flat - i_flat_0
            if f_flat == 0.0:
                flat = self.fs[i_flat_0]
            else:
                flat = (1-f_flat)*self.fs[i_flat_0] + f_flat* self.fs[i_flat_0+1]

            h5py.File("flat_a.h5","w")["data"]=flat
                
            if correct_flat :
                starts_r = np.array(range(0,im.shape[0]-nsize,nsize))
                starts_c = np.array(range(0,im.shape[1]-nsize,nsize))
                cor1 = np.zeros( [len(starts_r), len(starts_c) ]      ,np.float32 ) 
                cor2 = np.zeros( [len(starts_r), len(starts_c) ]      ,np.float32 )

                shift_finder = FlatFieldRealigner()

                for ir,r in enumerate(starts_r):
                    for ic, c in enumerate(starts_c):
                        cor1[ir,ic],cor2[ir,ic] = shift_finder.find_shift(  im[r:r+nsize, c:c+nsize]   , flat[r:r+nsize, c:c+nsize]  , low_pass = (1.0,0.3),  high_pass = (nsize, nsize*0.3 )   );

                cor1[np.isnan(cor1)] = 0
                cor2[np.isnan(cor2)] = 0

                cor1 =   correct_spikes( cor1, 3)
                cor2 =   correct_spikes( cor2, 3)

                h5py.File("cor1.h5","w")["data"]=cor1
                h5py.File("cor2.h5","w")["data"]=cor2
                
                cor1 = np.pad(cor1 ,((1,1),(1,1)), mode="edge")
                cor2 = np.pad(cor2 ,((1,1),(1,1)), mode="edge")
                
                print(starts_c +nsize*0.5) 

                xp = np.concatenate( [[0.0],  starts_c +nsize*0.5, [im.shape[1] ]] )
                yp = np.concatenate( [[0.0],  starts_r +nsize*0.5, [im.shape[0] ]] )


                x_ticks = np.arange(im.shape[1]).astype(np.float32)
                y_ticks = np.arange(im.shape[0]).astype(np.float32)

                foo=scipy.interpolate.interp2d(xp, yp, cor1, kind="cubic" )
                cor1 = foo(  x_ticks ,   y_ticks )

                foo=scipy.interpolate.interp2d(xp, yp, cor2, kind="cubic" )
                cor2 = foo(  x_ticks ,   y_ticks )


                h5py.File("cor1_interp.h5","w")["data"]=cor1
                h5py.File("cor2_interp.h5","w")["data"]=cor2


                # cor1 = CtfParameters("cors_inter_o.h5").cor1
                # cor2 = CtfParameters("cors_inter_o.h5").cor2

                
                unshifted_x, unshifted_y = np.meshgrid(np.arange( im.shape[1]).astype(np.float32) , np.arange(im.shape[0]).astype(np.float32)      ) 

                shifted_y = unshifted_y - cor1*0
                shifted_x = unshifted_x - cor2*0

                shifted_points = np.array([shifted_y, shifted_x]).swapaxes(0,2).swapaxes(0,1)

                flat = scipy.interpolate.interpn( (x_ticks, y_ticks), flat , shifted_points, bounds_error = False, fill_value = None  )


                h5py.File("flat_c.h5","w")["data"]=flat

                    
            im = im /flat


            h5py.File("im_flat.h5","w")["data"]=im

            
            
        if self.remove_spikes:
            print(" remove _spikes ", self.remove_spikes)
            h5py.File("im_spikes_before.h5","w")["data"]=im
            im = correct_spikes(im, self.remove_spikes)

            h5py.File("im_spikes.h5","w")["data"]=im


            

        if self.rh is not None:
            print ( self.rh.shape)
            print( nradio)
            myshift = self.rh[:,0,nradio]
        else:
            myshift = None

        im = self.pad_interpolate(  im    ,   myshift  )

        h5py.File("im_interp.h5","w")["data"]=im

        print( " DIVIDO PER ", im.mean())
        im = im/im.mean()


        h5py.File("im_mean.h5","w")["data"]=im
        
        return im

def correct_spikes(im, remove_spikes):
    if remove_spikes:
        # m_im = nabu_median_filter( im )
        m_im = scipy.signal.medfilt2d( im )
        fixed_part = np.array(im[ [0,0,-1,-1],[0,-1,0,-1]])
        
        where = (  abs( im - m_im ) > remove_spikes   )
        im[where] = m_im[where]
        im[ [0,0,-1,-1],[0,-1,0,-1]] = fixed_part
        
    return im
            
class ShiftPars(object):
            
    def __init__(self, oct_params, logger = None):

        self.logger = LoggerOrPrint(logger)

        self.shift                  = oct_params.shift_params.shift
        self.shift_approach         = oct_params.shift_params.shift_approach
        self.shift_constant         = oct_params.shift_params.shift_constant
        self.shift_first_plane_zero = oct_params.shift_params.shift_first_plane_zero
        self.shift_constant         = oct_params.shift_params. shift_constant
        self.shift_phased           = oct_params.shift_params.shift_phased 
        
        self.logger.info( f"Shift :  shift = {self.shift} ")

        assert self.shift==0,  f"In this version shift is not yet implemented, shift was {self.shift}"

class FilteringPars(object):
            
    def __init__(self, oct_params, logger = None):
        
        self.logger = LoggerOrPrint(logger)


        self.lim1 = oct_params.filtering_params.lim1
        self.lim2 = oct_params.filtering_params. lim2
        self.delta_beta = oct_params.filtering_params. delta_beta
        self.lim = None


    def retrieve_phase(self, im):

        h5py.File("im_mean.h5","w")["data"]=im

        f_im = np.fft.fft2(im)
        h5py.File("im_fft.h5","w")["data"] = f_im.real
        
        firec0      = self.ssq * f_im
        ssq_0_mean  = self.ssq[0,0]


        nf, mf = firec0.shape
        
        # ere it is assumed that the average of im is 1 and the DC component is removed
        firec0[0,0] -= nf*mf*ssq_0_mean


        h5py.File("ssq_p.h5","w")["data"] = self.ssq
        h5py.File("firec0.h5","w")["data"] = firec0

        
        ph = firec0 / ( 2*self.ssq*self.ssq + self.lim);


        h5py.File("delta.h5","w")["data"] = 2*self.ssq*self.ssq
        h5py.File("pfiltered.h5","w")["data"] = ph

        
        ph = np.fft.ifft2(ph).real
        
        h5py.File("presult.h5","w")["data"] = ph
        
        return ph
    
    def setup(self, geo_pars):
        nf = geo_pars.nf
        mf = geo_pars.mf

        
        fsamplex = geo_pars.le/geo_pars.pixelsize;
        fsampley = geo_pars.le/geo_pars.pixelsize;

        
        ff_index_x = np.fft.fftfreq( mf )
        ff_index_y = np.fft.fftfreq( nf )

        
        if mf%2 == 0 : # change to holotomo_slave indexing (by a transparent 2pi shift)
            ff_index_x[ ff_index_x  == -0.5    ] =  +0.5
        if nf%2 == 0 : # change to holotomo_slave indexing (by a transparent 2pi shift)
            ff_index_y[ ff_index_y  == -0.5    ] =  +0.5 

            
        self.ftot, self.gtot = np.meshgrid( ff_index_x* fsamplex ,   ff_index_y * fsampley )

        # fresnelnumbers and forward propagators
        distancesh = (geo_pars.z1h * geo_pars.z2h)/(geo_pars.z1h+geo_pars.z2h)
        distancesv = (geo_pars.z1v * geo_pars.z2v)/(geo_pars.z1v+geo_pars.z2v)

  
        self.betash = geo_pars.lambd*distancesh/ (geo_pars.le**2)
        self.betasv = geo_pars.lambd*distancesv/ (geo_pars.le**2)

        self.jo = None # this would be for the recursive reblur but it is not implemented yet

        ## -> cutn at first maximum of ctf largest distance
        self.cutn = math.sqrt(1./2/self.betasv)/fsampley;
        self.cutn = min(self.cutn, 0.5);

        self.logger.info( f"Normalized cut-off = {self.cutn:5.3f}")
        self.r = filt2(nf,1+self.cutn*nf,0.01*nf,mf); # division low and high frequencies
        self.lim = self.lim1 * self.r+ self.lim2 *(1-self.r)

        self.setup_app1()
        
    def  setup_app1(self):
        if ( self.delta_beta ):
            
            self.logger.info("Taking into account absorption assuming homogeneous object.\n"
                             "Set delta_beta = 0 if this is not the purpose.")
            ssq  = ssquare2(self.betash, self.ftot, self.gtot, 1, self.betasv)+1.0/self.delta_beta*csquare2(self.betash, self.ftot, self.gtot, 1, self.betasv );
            
        else:
            
            ssq  = ssquare2(self.betash, self.ftot, self.gtot, 1, self.betasv )
            
        self.ssq = ssq
            

class  ID16(object):
    def __init__(self,
                 oct_params,
                 le = 10.0e-6     ,
                 logger = None):
        """
        le : characteristic lengthscale of the object to make problem dimensionless
        """
        self.oct_params  = oct_params
        
        self.logger = LoggerOrPrint(logger)
        self.geo_pars   = GeoPars(oct_params, le = le)
        self.shift_pars = ShiftPars(oct_params)
        self.filtering_pars = FilteringPars(oct_params)
        self.detection_corrector  = None
        
        self.setup()

                        
    def set_corrections(self, rh = None, dark = None, fs=None, refns = [], remove_spikes = 0.0):
        self.detection_corrector = DetectionCorrector(rh=rh, dark = dark, fs = fs, refns = refns,
                                                      remove_spikes = remove_spikes, geo_pars = self.geo_pars ) 

    def miscellanea_check(self, **args):
        for key,val in args.items():
            if key in self.oct_params.miscellanea:
                if issequenceforme( val ) :
                    return abs(np.array(self.oct_params.miscellanea[key]) - np.array(val)).sum()==0
                if self.oct_params.miscellanea[key] != val:
                    raise ValueError( f"key {key} if given must be {val}"  ) 
                    
        
    def setup( self):

        self.pad_method = None        
        if (self.geo_pars.mf,self.geo_pars.nf)!=(self.geo_pars.m, self.geo_pars.n):
            
            self.logger.info( f"Padding : needs to extend the image")
            
        #  con gli extra 3 e sarebbero extra 5 se is360 ???")


        self.miscellanea_check(
            correct_detector=3,
            planes = 1,
            correct_shrink = 0,
            approach = 1,
            recursive = 0,
            var_cut = 1 ,   # would define cutn
            subtr_filtered_mean =0,
            centralpart_shift = [0,0]
        )        
        
        assert self.oct_params.miscellanea.correct_detector==3, "only correct_detector=3 is implemented "
        
        assert self.oct_params.miscellanea.planes == 1 , " only planes = [1] is implemented  "
        
        self.filtering_pars.setup( self.geo_pars ) 




##  r .*= filt2(n, n*high_cut_off, n*high_width, m);

def filt2(n, cutn, sigman, m=None, cutm=None):
    if m is None:
        m = n
    if cutm is None:
        cutm = (cutn*m)/n
        
    x = np.fft.fftfreq( m ) *m
    y = np.fft.fftfreq( n ) *n
    
    x,y = np.meshgrid( x, y  )
    rnorm_x = x*cutn/cutm    
    r   = np.sqrt( y*y + rnorm_x*rnorm_x) 

    r = erfc(  (r-cutn)/sigman  )
    
    return r/r[0,0]


# ssq=ssquare2(beta,f,g) returns sin(pi*beta*(f^2+g^2))
# beta is equal to lambda*D/l^2, i.e. the Fresnelnumber
# for the complete spectrum (from 0 to fsample)
# f_isall = 0
# only f and g from 0 to fsample/2 should be given, the other part is 
# calculated using the periodicity in frequency-space
# f_isall = 1
# complete frequency range is given
# allows for astigmatism (5th argument)
def ssquare2(betah,f,g,f_isall=0, betav=None):
    return trigo2( np.sin, betah,f,g,f_isall, betav)
def csquare2(betah,f,g,f_isall=0, betav=None):
    return trigo2( np.cos, betah,f,g,f_isall, betav)

def trigo2(foo, betah,f,g,f_isall, betav):
    if betav is None:
        betav = betah        
    if not f_isall:
        raise ValueError(" f_isall in ssquare2 must be se to 1 for now. "  ) 
    ssq=foo((np.pi*betah)*(f*f)+(np.pi*betav)*(g*g) )
    return ssq


    
class FlatFieldRealigner(AlignmentBase):
    def find_shift(
        self,
        img_1: np.ndarray,
        img_2: np.ndarray,
        peak_fit_radius=1,
        high_pass=None,
        low_pass=None,
    ):

        self._check_img_pair_sizes(img_1, img_2)
        
        if peak_fit_radius < 1:
            self.logger.warning("Parameter peak_fit_radius should be at least 1, given: %d instead." % peak_fit_radius)
            peak_fit_radius = 1

        img_shape = img_2.shape
        roi_yxhw = self._determine_roi(img_shape, None)

        img_1 = self._prepare_image(img_1, roi_yxhw=roi_yxhw, median_filt_shape=None)
        img_2 = self._prepare_image(img_2, roi_yxhw=roi_yxhw, median_filt_shape=None)

        cc = self._compute_correlation_fft(img_1, img_2, padding_mode=None, high_pass=high_pass, low_pass=low_pass)
        img_shape = img_2.shape
        cc_vs = np.fft.fftfreq(img_shape[-2], 1 / img_shape[-2])
        cc_hs = np.fft.fftfreq(img_shape[-1], 1 / img_shape[-1])

        (f_vals, fv, fh) = self.extract_peak_region_2d(cc, peak_radius=peak_fit_radius, cc_vs=cc_vs, cc_hs=cc_hs)
        fitted_shifts_vh = self.refine_max_position_2d(f_vals, fv, fh)

        return fitted_shifts_vh 



    @staticmethod
    def refine_max_position_2d(f_vals: np.ndarray, fy=None, fx=None):
        if not (len(f_vals.shape) == 2):
            raise ValueError(
                "The fitted values should form a 2-dimensional array. Array of shape: [%s] was given."
                % (" ".join(("%d" % s for s in f_vals.shape)))
            )
        if fy is None:
            fy_half_size = (f_vals.shape[0] - 1) / 2
            fy = np.linspace(-fy_half_size, fy_half_size, f_vals.shape[0])
        elif not (len(fy.shape) == 1 and np.all(fy.size == f_vals.shape[0])):
            raise ValueError(
                "Vertical coordinates should have the same length as values matrix. Sizes of fy: %d, f_vals: [%s]"
                % (fy.size, " ".join(("%d" % s for s in f_vals.shape)))
            )
        if fx is None:
            fx_half_size = (f_vals.shape[1] - 1) / 2
            fx = np.linspace(-fx_half_size, fx_half_size, f_vals.shape[1])
        elif not (len(fx.shape) == 1 and np.all(fx.size == f_vals.shape[1])):
            raise ValueError(
                "Horizontal coordinates should have the same length as values matrix. Sizes of fx: %d, f_vals: [%s]"
                % (fx.size, " ".join(("%d" % s for s in f_vals.shape)))
            )

        fy, fx = np.meshgrid(fy, fx, indexing="ij")
        fy = fy.flatten()
        fx = fx.flatten()
        coords = np.array([np.ones(f_vals.size), fy, fx, fy * fx, fy ** 2, fx ** 2])

        coeffs = np.linalg.lstsq(coords.T, f_vals.flatten(), rcond=None)[0]

        # For a 1D parabola `f(x) = ax^2 + bx + c`, the vertex position is:
        # x_v = -b / 2a. For a 2D parabola, the vertex position is:
        # (y, x)_v = - b / A, where:
        A = [[2 * coeffs[4], coeffs[3]], [coeffs[3], 2 * coeffs[5]]]
        b = coeffs[1:3]
        vertex_yx = np.linalg.lstsq(A, -b, rcond=None)[0]

        vertex_min_yx = [np.min(fy), np.min(fx)]
        vertex_max_yx = [np.max(fy), np.max(fx)]
        if np.any(vertex_yx < vertex_min_yx) or np.any(vertex_yx > vertex_max_yx):
            self.logger.warning("Fitted (y: {}, x: {}) positions are outside the input margins y: [{}, {}], and x: [{}, {}]".format(
                    vertex_yx[0], vertex_yx[1], vertex_min_yx[0], vertex_max_yx[0], vertex_min_yx[1], vertex_max_yx[1],
            ))
            return math.nan, math.nan

        return vertex_yx



def centered_cut(im, new_n_r, new_n_c):    
        # we recut around the center and return

    n_r, n_c = im.shape
    
    center_r = (n_r-1)/2
    center_c = (n_c-1)/2
    
    rb = int(round(0.5+center_r- new_n_r/2))
    re = new_n_r + rb 
    
    cb = int(round(0.5+center_c -new_n_c/2))
    ce = new_n_c + cb

    return im[rb:re, cb:ce  ]
    

def issequenceforme(obj):
    if isinstance(obj, str):
        return False
    return isinstance(obj, collections.abc.Sequence)

if __name__ == "__main__":
    from nabu.preproc.ctf_parameters import CtfParameters
    oct_params  = CtfParameters("old_params.h5")
    print(oct_params)
    id16 = ID16(   oct_params)


    oct_bigdata = CtfParameters("bigdata.h5")

    
    
    id16.set_corrections( rh = oct_bigdata.rh ,
                          dark = oct_bigdata.flats_dark.dark,
                          fs = oct_bigdata.flats_dark.fs,
                          refns = oct_bigdata.flats_dark.refns.flatten(),
                          remove_spikes = 0.04
    ) 


    oct_im = CtfParameters("images.h5")


    im = id16.detection_corrector.apply_corrections( oct_im.im,   int(oct_im.np), nsize = 100, correct_flat = 1)

    phase = id16.filtering_pars.retrieve_phase(im)
    phase = id16.detection_corrector.recut( phase )
            
    edf = fabio.edfimage.EdfImage()
    edf.data = phase
    edf.write(f"phase{int(oct_im.np):04d}.edf")

    diff = id16.filtering_pars.ftot - oct_bigdata.ftot
    print( "maxdiff for ftot ", abs(diff).max())
    h5py.File("ftot.h5","w")["diff"] = diff
    
    diff = id16.filtering_pars.gtot - oct_bigdata.gtot
    print( "maxdiff for ftot ", abs(diff).max())

    
    h5py.File("ftot.h5","r+")["rp"]    = id16.filtering_pars.r
    h5py.File("ftot.h5","r+")["ro"]    = oct_bigdata.r
    diff  = oct_bigdata.r - id16.filtering_pars.r
    h5py.File("ftot.h5","r+")["rdiff"] = diff
    print( "maxdiff for r", abs(diff).max())

    ssq = CtfParameters("ssq.h5")
    diff  = ssq.ssq - id16.filtering_pars.ssq
    print( "maxdiff for ssq", abs(diff).max())
    h5py.File("q.h5","w" )["ssq1"] = ssq.ssq
    h5py.File("q.h5","r+")["ssq2"] = id16.filtering_pars.ssq
    h5py.File("q.h5","r+")["diff"] = id16.filtering_pars.ssq - ssq.ssq
    
    insets = CtfParameters("findshft_data.h5")
    
    finder = FlatFieldRealigner()
    shifts = finder.find_shift( insets.im, insets.f , low_pass = (1.0,0.3),  high_pass = (100, 30.0 ) )
    print(" TROVATO ")
    print( shifts ) 

    
