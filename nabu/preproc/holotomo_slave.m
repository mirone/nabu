## Copyright (C) 2006 P. Cloetens
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

## holotomo_slave

## Author: P. Cloetens <cloetens@esrf.fr>
##
## 2006-06-16 P. Cloetens <cloetens@esrf.fr>
## * Initial revision
## 2006-07-21 PC
## * change method of imresize from bilinear to linear (same in reality)
## 2006-07-28 PC
## * open complete image in ImageJ instead of sub-part
## * does tomo (dark.edf refHST) for all the individual scans if not yet done
## 2006-07-31 PC
## * allows for different z1v and z1h: in case of astigmatic wavefront; effect of monochromator?
## * correction in mcf2 and way it is called
## 2006-08-03 PC
## * allows for different naming convention when 'numbers' does not exist or is empty (suggestion PT)
## * 
## * namestr itself is used as full name(s) (can be a cell array)
## * for usual naming convention: resultdir is now namestr_ instead of namestr
## * for unusual naming convention: resultdir is first namestr + 'ht_'
## * creates simple info-file
## * creates slice.par for reconstruction single slice
## * creates .par + parcut for reconstruction volume: same volume as reference_plane if existing or 'total' if nothing previously selected
## 2006-10-02 PC
## * check that rhapp.mat file exists when shift = 0 (shifts should be determined at least once)
## * introduce the possibility to determine low frequencies by extrapolation (see lowfreqc2 and ext_lowfreq)
## * edges to zero line by line (see free_edge = number of free columns)
## * implemented using the function find_phase
## * introduce possibility to pad the images to the next power of 2 or more
## 2006-11-08 PC
## * introduce the possibility to save the projections or not (save_projection)
## 2007-01-31 PC
## * correct bug in roi selection for *.par file (indicated by PT)
## * start/end voxels were not correct in case of an roi on phase maps
## * zone of overlap between roi for phase map and roi in original *.par file is used now
## 2007-02-09 PC
## * add possibility to do padding of projections (pad_images)
## * pad_images can be [2048 2048] or 1 -> pad to first power of 2; 2 -> next power of 2, etc
## * last column/row value is reproduced (extend)
## * possibility to average a number of rows or columns (extend_avg = 20)
## * add possibility to apply a filter after phasing (post_filter or lim2 if not numerical)
## * add possibility to estimate low frequency part of phase from absorption (delta_beta)
## * calc_residu = 1 -> prepares propagator etc to estimate delta_beta with test_d_b
## * number of iteration in mixed approach can be set (nloop_mixed = 3)
## * add possibility to subtract filtered_mean.edf files from the projections (subtr_filtered_mean)
## * add blurring to I0 in case of approach 4
## * blurring can by gaussian type (ssigma > 0) or erf type (ssigma = 0)
## * add more information to the info file
## * assure that projection size n and m are multiples of 4 in case magnification=1 and centralpart=1
## * separate files for initialization and body of different approaches: ht_initialize_appx.m and ht_appx.m
## * default values for pad_images, save_projection, approach, lim1, lim2, post_filter, delta_beta, calc_residu, nloop_mixed
## 2007-03-14 PC
## * order of polynomial fits can be set ( shift_polyfit_order_h = 3 ; shift_polyfit_order_v = 2)
## * order of polynomial fit is reduced if data is insufficient
## 2007-04-25 PC
## * reduce memory usage (avoid some variables, approach 4 -> 1st plane ssq etc is not used)
## * shift determination: in case of findshift restrict to the original part of the image (padding is not a good idea)
## * approach 3: introduce possibility to use delta/beta
## * approach 3: for final deconvolution, replace wiener with 1/(delta+lim) (this corresponds more to real Wiener, otherwise some kind of reblurring)
## * rotation axis determination: check if result is consistent with offset for reference_plane
## 2007-05-21 PC
## * with approach 3 and 4 test semi-automatically best value of delta_beta (specify vector with values to test)
## * check_z1v searches best value of z1v using deform_vert_rs
## * check_z1v = 1 uses planes nb-1 and nb or specify planes to use with check_z1v = [2 3]
## * default values for correct_flat, remove_spikes, save_radiographs, cylinder, constrain, recursive
## * default values for save_absorption, show_projections, projections, check_z1v
## * default values for centralpart, shift_constant, shift_show, shift_approach, shift_constrain
## * default value of pad_images changed to 1
## * use pyhst_parfile instead of hst_parameter -> rotation axis to center by default and no confusion with correct_spikes_threshold !!!
## * possibility to show phase map (and absorption map) using show_projections = 1
## * shifts are not shown if a single projection is processed
## 2007-05-28 PC
## * foresee manual alignment of radiographs with shift_approach = 3 (refinement included)
## 2007-06-05 PC
## * RCS 1.2
## * display aligned radiographs with individual contrast, shift_show_contrast (default 1)
## * default show_projections set to 1
## 2007-06-07 PC
## * RCS 1.3
## * correct bug where variable planes was needed but undefined in the case shift=1 and subtr_filtered_mean
## * possibility to correct for ccd defects (correct_ccd and correct_ccd_par)
## * possibility to correct for image distortion (correct_distortion and correct_distortion_par{1:nb})
## 2007-07-31 PC
## * introduce preserve_offset (do not set mean edges to zero when delta_beta is not zero)
## 2007-10-18 PC
## * RCS 1.4
## * default value for vers ( '' ) 
## * attempt to introduce shift_singleplot ( all plots in 1 window ), to be improved
## * introduce approach 7 ( symmetric mixed approach )
## * save in rhappnofit.mat rh (and not rhapp)
## * use image 0000 for rotation axis determination and not nvue+2
## * axisposition sets different options for rotation axis determination ( 'global', 'accurate', 'manual' ) ( default 'global' )
## * do not compare result to reference_plane ( can be different due to cut and magnification )
## * count distance with respect to first plane if absorption image is required
## * translate images to better than 1/10 pixel
## * introduce approach 8 for testing with personal ht_initialise_app8 and ht_app8
## 2007-12-06 PC
## * RCS 1.5
## * start include case single distance
## * define r, separation low/high frequencies
## * correct call to ht_initialize_app8
## * changes to approach 3:
## *    foresee case preserve_offset
## *    add possibility to test parameter lim1 with minimum sigma background, calls test_lim1
## *    add Lcurve test for lim1 and lim2
## *    more correct determination of absorption part (low frequencies can be retrieved, no lim1 needed!)
## *    correct case strong_absorption (division by average intensity)
## *    determine snorm as mean of average intensity for different distances (instead of first distance only)
## *    case delta/beta: use low frequencies of retrieved absorption map instead of first image
## * default values for test_par_polyfit_order, Lcurve
## 2008-01-10 PC
## * RCS 1.6
## * changes to approach 5
## *    add approximate TIE solution with two distances (see Bronnikov)
## *        (simple_approach = 1; single_index is index of phase image)
## *    add approximate TIE solution with one distance (homogeneous object)
## *        (simple_approach = 1; give scalar for numbers)
## *    simple_approach (Bronnikov or homogeneous object) is compatible with delta_beta
## *    in case of single distance and delta_beta -> lim1/2 is not used
## *        (delta_beta plays this role as lim = 2/delta_beta)
## * set default values for approach 5 of simple_approach (= 0) and single_index (= 2)
## * set value of nloop_mixed also for approach 8 (tests)
## * correct bug in manual determination rotation axis (image was not flipped)
## 2008-04-02 PC
## * RCS 1.7
## * make compatible with scan over 360 degrees
## * use correct_motion_calc to calculate motion correction of reference scan
## * foresee using images during or after scan for rotation axis determination
## *    uses axisfilesduringscan or FT_AXISFILESDURINGSCAN
## *    phase maps after scan are probably wrong for the moment
## * put fitting of shifts in a separate function ht_fitshifts
## *    can be called independently to change some shift values
## * introduce variable cut-off frequency to separate low/high frequencies
## *    set with variable var_cut ( default 1 )
## * ht_app3:
## *    different prior estimate (filter with r)
## *    call test_parameters instead of test_lim1 and test_delta_beta
## * ht_app4:
## *    introduce L-curve method for lim1
## *    slightly different prior estimate
## *    scan lim1 and test_parameters
## * ht_app7:
## *    introduce L-curve method for lim1
## *    different prior estimate (filter with r)
## *    scan lim1 and test_parameters
## 2008-04-07 PC
## * RCS 1.8
## * foresee different methods for padding
## *    set with pad_method ( default: 'reflect' )
## * correct bug, argument interactive for ht_fitshifts had no default
## *    set now with shift_interactive ( default 0 )
## * initialize lim_test and delta_beta_test in common part
## *    make length(lim1) equal to length(lim2)
## *    they are changed simultaneously
## * more possibilities for variable cut-off frequency
## *    var_cut = 0 -> old case, normalized cut-off frequency cutn = 0.1
## *    0 < var_cut < 1 -> cutn = var_cut
## *    var_cut = 1     -> cutn at first maximum of ctf largest distance
## *    var_cut > 1 (& delta_beta > 0)  -> small cutn such that phase contrast = abs contrast 
## 2008-04-08 PC
## * RCS 1.9
## * order lim2 when it is tested alone (single value lim1)
## * ht_app3
## *    restrict calculation of model/regularization error to field of view
## *    do not consider DC component of ME
## *    when lim2 is scanned alone and Lcurve = 0 -> show ssq error with respect to most blurred image
## * ht_app4
## *    foresee simultaneous scanning of lim1 and lim2
## *    restrict calculation of model/regularization error to field of view
## *    do not consider DC component of ME
## *    when lim2 is scanned alone and Lcurve = 0 -> show ssq error with respect to most blurred image
## *    foresee Lcurve method on lim2 alone (not very useful)
## * ht_app7
## *    foresee simultaneous scanning of lim1 and lim2
## *    restrict calculation of model/regularization error to field of view
## *    do not consider DC component of ME
## *    when lim2 is scanned alone and Lcurve = 0 -> show ssq error with respect to most blurred image
## *    foresee Lcurve method on lim2 alone (not very useful)
## * test_parameters
## *    normalize delta_beta for better numerical precision in fit
## *    foresee scaaning of lim1 and/or lim2
## 2008-04-09 PC
## * RCS 1.10
## * change default value of preserve_offset to 1 when delta_beta is not zero
## 2008-05-19 PC
## * RCS 1.11
## * remove bugs when saving radiographs of original scan
## *    default value for save_radiographs_slice
## *    save images before phase retrieval because ht_appx changes meaning of s{k}
## 2008-06-25 PC
## * RCS 1.12
## * correct for modified definition of mkdir
## 2008-11-26 PC
## * RCS 1.13
## * ht_app1
## *    make approach 1 compatible with testing of lim1, lim2 and Lcurve
## *    make approach 1 compatible with KB imaging (resolution envelope depends on plane)
## *    introduce modified_delta ( saved version of delta with zeros removed for example, default 0 )
## * ht_app3
## *    clean-up approach 3 ; calculate absorption for Lcurve method
## *    make approach 3 compatible with KB imaging (resolution envelope depends on plane)
## *    introduce modified_delta ( saved version of delta with zeros removed for example, default 0 )
## * ht_app4
## *    approach 4: allow simultaneous testing of delta_beta and lim1 (uses ht_Lcurve_2D)
## * ht_app6
## *    introduce approach 6: paraboloid method (PAM); no phase unwrapping
## *    compatible with KB imaging and parallel beam
## *    introduce modified_delta ( saved version of delta with zeros removed for example, default 0 )
## * default value for magnification (1)
## * introduce magnification_large ( 0 -> "parallel beam"; 1 -> "projection" )
## * adapt code in general to projection case
## * possibility to process radiographs; set radio_only = 1 ( default 0 )
## * for motion of reference_plane, correct for magnification
## * correct for ccd immediately after reading each image (correct_ccd, for FReLoN Kodak 4M)
## * introduce image_threshold for shutter problem ( default 0 )
## *    when not zero, image mean should be at least image_threshold,
## *    otherwise average of next good image on left and right are used
## * activate recursive part in case of approach 1
## * shift determination
## *    shift_approach 2 and 3 -> use previous values in rhapp 
## *        as initial guess when shift_useprevious = 1 ( default 1 )
## *    iterative correction of the shifts through rhapp_iter.mat
## *        (determined with recursive part and shift_iterative = 1)
## 2009-12-02 HSu
## * Modified to write all the parameters to the resulting edf-files.
## 2009-12-03 HSu
## * Modified to write the shift_show images in .edf instead of jpg
## 2010-05-31 PC
## * introduce recursive_n and recursive_m ; used by ht_recursive to restrict the region that is optimized
## 2010-06-29 PC
## * plot results of quali_series in microns and suggest reference_plane
## 2010-11-08 HSu
## * fixed weighting of reference images for the last part of the scan
## * pos_feature now not obligatory parameter
## * now has ability to run with a single distance
## 2010-11-17 HSu
## * Corrected a bug in the weighting of reference images 
##   (division by zero if nvue was divisible by refon)
## 2010-12-16 HSu
## * Added recursive possibility for approach 8
## 2010-12-17 PC
## * Correct in a temporary way inconsistencies between size of rhapp and ref_h/ref_v
## * Change default pad_method to 'reflect', switched back to 'extend' for fft based cross-correlation 
## 2010-12-21 PC
## * Add pixel_size (at sample) to the header
## 2011-01-10 PC
## * Leave shift_polyfit_order empty if not specified; default is set by ht_fitshifts to [6 2]
## * Set default value of recursive_save_iteration
## * Make sure no subplots are present after shift determination
## * Display pixel size of phase map
## 2011-02-06 PC
## * ht_app1
## *    easy testing of multiple parameters lim1, lim2, delta_beta; stores data for reconstruction of one slice
## * create par-files for testing of parameter
## 2011-02-06 PC
## * introduce possibility object_prior
## * introduce possibility to give centralpart as explicit region [512 2048] or size
## * introduce use_shrink_list (shrinkage from quali_series)
## * !!! correct bug in measuring distance with respect to first plane, do this only for approach 4, 5 and 7 with multiple distances
## * calculate snorm for recursive_purephase == 2
## * recursive_scratch can be prefix of image to use as initial guess
## * be more consistent in having im as amplitude when starting recursive part (approach 1 and 8)
## 2011-08-29 PC
## * set value of show_projections and shift_show depending on isinteractive
## * avoid shift_approach to be equal to 3 in batch mode
## * mainly to avoid calling imagej in batch mode
## 2011-10-05 HSu
## * add the possibility to do single projection data with flat-field correction
## * add force_parfiles to allow forcing the creation of parfiles (skips phase retrieval)
## 2011-12-21 PC
## * add possibility to resume without recalculating everything, for use with besteffort OAR queue
## * add variable function_filename
## 2012-06-05 HSu
## * refactoring: image treatment parts as separate scripts
## * add possibility to use a previous phase image for calculating the sample distortions
## 2012-07-18 HSu
## * added possibility to give refraction map name in refmapvers (default pag_)
## * made that shift is not made 0 in batch mode (see htbatch macros too)
## 2012-09-24 HSu
## * add real padding
## 2012-09-25 PC
## * consistent use of planes
## 2012-09-29 PC
## * add possibility shift_approach = 5 using ij_turboreg
## * make pad_consistent the default
## * introduce recursive_mask_padded for ht_recursive
## 2012-10-03 PC
## * remove access to hb, vb parameters; assume hb = vb = 1
## 2012-10-04 PC
## * add no_sinogram_filtering option when needed to par-files
## 2012-10-06 PC
## * set default values for recursive_attenuate and recursive_exclude (for ht_recursive)
## 2012-10-14 PC
## * correct mix of nf/mf in cut
## 2012-10-23 HSu
## * fix a problem that caused radiographs not to be saved at all
## 2012-11-04 PC
## * correct typo in 'recursive_exclude'
## * set default values for recursive_mask and recursive_constrain_grad (for ht_purephase)
## * change display of shifts
## * change default value of use_real_padding to 1
## * set recursive_mask_padded equal to (use_real_padding >= 1)
## * set pad_consistent equal to (use_real_padding >= 1);
## * pad_images = 1 -> pad to 2*(power of two >= original image size)
## * make holotomo_slave_version_pad the default version; old holotomo_slave is holotomo_slave_20121104
## 2013-03-12 PC
## * correct call of ht_oar_resume (use resultprefix for single distance case)
## * moved further down as a consequence
## 2014-03-14 PC/MH
## * allows to create .par when using recursive_save_iteration
## * new variables centralpart_shift_v and centralpart_shift_h to move the center of centralpart (default is 0)
## 2014-11-25 PC/AP/FF
## * add possibility to do whitefield correction (detector only)
## * add shift_useprevious possibility to shift_approach 5 (ij_turboreg)
## * correct code for shift_approach 4 (normxcorr2)
## * use of pixelsize_detector
## * introduce remove_vertical_background and weight_threshold
## * directory names corrected in case of radiography only
## * magnifications Mh and Mv left at 1 except if magnification & magnification_large
## * update comments describing different approaches
## 2014-12-10 PC/MH
## * add shift_first_plane_zero to keep the axis of rotation of the first plane (default is 1)
## * create pyhst par-files as well for testing delta_beta values in approach 8
## 2014-12-17 PC/HS!
## * put paganin_multiple_distance = 1 as default
## 2014-12-18 PC
## * avoid setting lim2 equal to gaussian_filter in case of approach 8
## * avoid display mean in case of shift_first_plane_zero
## 2015-02-01 PC
## * do not transpose in whitefield correction -> transpose needed here and in ht_read_images
## * possibility to modify resultdir with resultdir_prepend or resultdir_append
## 2015-04-23 PC
## * apply centralpart for shift determination the same way for all shift approaches
## * add shift_low_cut_off with default 1 (high pass filter) before DFT shift determination
## 2015-06-19 PC/AP
## * Fix bug in determination if image needs padding or not after magnification correction
## 2015-07-20 PC
## * change names of directories when testing parameters (param_%02d_)
## * make correct_detector = 0 do nothing
## * significant changes in ht_app1 and ht_app8 (see there)
## 2015-08-23 PC
## * normalize images to median of different distances (protect against refill)
## 2015-09-23 PC+MH
## * introduce random_disp to account for random displacements during scans
## * keep inside ht_fitshifts and rhapp*.mat values as if there was no random displacement (smooth and fittable)
## * in holotomo_slave correct rhapp for known displacement from correct.txt files
## 2015-10-06 PC
## * add shift_high_cut_off with default 0 (low pass filter) before DFT shift determination
## * apply low/high cut_off also in case of DFT of shift_approach = 1
## 2015-10-13 PC
## * add variable debug (default 0) for extra display
## * add variable find_abs_plane (default 1) to select absorption image, -1 creates pseudo-absorption
## * calculate magnifications Mh and Mv if magnification or magnification_large are non zero
## * display of effective pixel size
## 2015-10-15 PC
## * for approach 8 and recursive_scratch = 1, put only phase to zero, not amplitude
## * introduce variable delta_beta_normalised (default 0)
## 2016-01-07 PC
## * introduce do_total_mean (average all projections after flatfield and similar corrections)
## * adapt size of rand_disp to the one of rhapp
## * add warnings in case some corrections are set but not applied because do_flatfield = 0
## 2016-03-04 PC
## * adapt calls of quali_series to random_disp and new options
## 2016-03-24 PC
## * change call of quali_series
## *    shift_approach and centralpart are identical to those of holotomo 
## *        (no use of global variables FT_CENTRALPART and FT_CORRELATEMETHOD)
## *    debug in quali_series can be set with reference_quali_debug ( default 2 )
## 2016-03-29 PC,AP,IZ
## * solve rounding issue in index_180
## * put use_shrink_list as default
## 2016-07-25 PC
## * Make m-file for 2D phase retrieval as simple as possible
## * No longer needed to provide, projections, nvue, n, m, do_flatfield, correct_detector
## *    oversamp, reference_quali, reference_moved, reference_plane
## * do_flatfield defaults to 0 in case of radio_only 1, it defaults to 0 otherwise
## * avoid figures in case of radio_only != 0
## 2016-07-31 PC
## * Run tomo also with shift = 0
## 2016-10-10 PC
## * Add default for variable shift_distrib ( default 0 )
## * Save distributed shift files when relevant
## * Avoid continuation marker \
## * Clean up spaces and tabs
## * Allow shift file with more distances than used in phase retrieval
## 2016-10-10 PC
## * Add possibility to use different padding methods in the vertical and horizontal direction
## 2016-10-28 PC
## * Make sure output of bwdist is double; it is single in Octave version >3.8. Critical for a not clear reason.
## 2016-12-20 PC
## * Introduce cropping to different sizes using ncrop, mcrop
## * Allow to give files in recursive_scratch for approach 8 (related to illumination/object mixing)
## * Preserve information from par-files individual scans or not
## * Correct for jumps in reference plane using rhjumps.mat
## * Move all padding to ht_do_padding
## * Introduce recursive_reblur to reblur with jo or not the error image
## 2017-01-06 PC
## * Remove again rhjumps; assume everything is already in rhapp
## 2017-03-06 PC
## * for save_radiographs, take into account ncrop, mcrop
## * for rotation axis calculation, take into account ncrop, mcrop
## * introduce pad_last_from_forward with default 0
## 2017-09-29 PC
## * allow shift_estimate to be a string with coarse method for findshift
## * create directories for volumes if needed (volfloat, volraw)
## 2017-11-24 PC
## * pass correct_whitefield to quali_series when relevant
## 2018-02-01 PC
## * use ht_random_disp_read to read random displacement as stored by acquisition with checks
## * use fullfile as much as possible, avoid '/' at the end of names
## 2018-02-01 PC
## * correct call of fullfile
## * use ht_filename_image for format filename
## * use strcat instead of sprintf to concatenate strings
## 2018-02-02 PC
## * correction when prefix is not a cell
## 2018-02-14 PC
## * add post_crop_filter (post filter but after cropping)
## 2018-05-10 PC
## * add image_positive ( default 1 ) to avoid negative values in images and refs
## * introduce shrink_centerv/h
## * put negative value for angle_between_projections
## 2018-06-22 PC
## * make shift_estimate work with shift_approach 3 and 5
## 2018-07-06 PC
## * do not force shrink to be positive
## * introdice correct_shrink_positive ( default 0 ) to force positive shrinks
## * display message when shrink values are forced to be positive
## 2018-10-08 PC
## * in minimum file size for ht_oar_resume take into account ncrop/mcrop
## 2018-10-22 PC
## * remove call to parcut (unused)
## 2018-11-21 PC
## * reorganize shift_estimate and shift_useprevious
## * introduce shift_approach = 7 (maximization of energy phase with respect to shifts)
## 2018-12-12 PC
## * introduce pad_normalize_exterior for selection how normalization is done when using real padding ( defaut 1 )
## * merge two if-blocks (apparently identical conditions)
## * calculate forward_prop and forward_r for pad_last_from_forward = 2
## 2019-01-11 PC
## * use consistent cropping, centered vertically on last plane if field of view extension is used
## * uses parameters ncrop_consistent and mcrop_consistent
## * variable name pad_extend_avg (was extend_avg)
## * initialize variables pad_blend and pad_blend_last
## * save packed HT parameters as [resultprefix '.mat']
## * small cleaning
## 2019-01-17 PC
## * clean intermediate phase map to save disk space (two_pass)
## * introduce corresponding variable recursive_scratch_remove ( default 0 )
## * initialize two_pass to 0
## 2019-07-03 PC
## * add centralpart_shift_h_func to make centralpart_shift_h angle dependent
## * wrap centralpart parameters in a structure for easy use by ht_shift_app7 for example
## * move shift_iterative related initializations to ht_initialize_recursive
## 2019-07-15 PC
## * initialize here correct_flat_par, normalize_background
## * correct_flat 0 -> empty; correct_flat string -> function handle
## * goes with cleanup of ht_do_flatfield and introduction of ht_correct_flat_old
## * normalize_background as vector of length 4 is turned into a function handle
## 2019-08-16 PC
## * change default value of shift_estimate to [] (was [0 0])
## * use is_valid_func instead of ischar to check validity of shift_estimate as a function
## 2020-05-02 PC
## * Replace obsolete flipdim with flip
## 2020-05-08 PC
## * preserve_offset as logical true/false, &&
## * clear variables when possible
## * define [ im = abs(trec); ] if needed
## * rotation axis determination: try/catch reading of projections
## * & -> &&
## * introduce shift_iterative_save_indiv_mat
## * true/false instead of 1/0 for some parameters
## * correct rh_mean in case of radio_only

global IMAGEJ_STARTED FT_AXISFILESDURINGSCAN

#################
# check if program runs interactively
# determines if graphics will be output
interactive = isinteractive;
interactive = 0;

rewrite_big = 0

###########
# setting default values for undefined variables
if !exist('debug','var')
  debug = false;
endif
if !exist('radio_only','var')
  radio_only = false;
endif
if !exist('projections','var')
  projections = [];
endif
if !exist('nvue','var')
    nvue = [];
endif
if radio_only
    if !exist('n','var')
        n = header(fullfile(dir, namestr, ht_filename_image(namestr, radiographs(1))), 'Dim_2', 'integer');
    endif
    if !exist('m','var')
        m = header(fullfile(dir, namestr, ht_filename_image(namestr, radiographs(1))), 'Dim_1', 'integer');
    endif
else
    if !exist('n','var')
        dir_1_ = [namestr "_1_"];
        n = header(fullfile(dir, dir_1_, ht_filename_image(dir_1_, 0)), 'Dim_2', 'integer');
    endif
    if !exist('m','var')
        dir_1_ = [namestr "_1_"];
        m = header(fullfile(dir, dir_1_, ht_filename_image(dir_1_, 0)), 'Dim_1', 'integer');
    endif
endif
if !exist('ncrop','var')
    ncrop = n;
endif
if !exist('mcrop','var')
    mcrop = m;
endif
if !exist('ncrop_consistent','var')
    # crop centered on last plane if field of view extension is used
    ncrop_consistent = (ncrop > n) ;
endif
if !exist('mcrop_consistent','var')
    # crop centered on image by default because of center of rotation
    mcrop_consistent = false;
endif
if !exist('do_flatfield','var')
    do_flatfield = !radio_only;
endif



if !exist('correct_flat','var')
    correct_flat = [];
endif
if !exist('correct_flat_par','var')
    correct_flat_par = [];
endif

printf([ correct_flat])
printf("-----------\n");
disp( size( correct_flat_par) )
printf([ correct_distortion])
disp(  correct_distortion_par )

if !isempty(correct_flat)
    if ischar(correct_flat)
        correct_flat = str2func(correct_flat);
    elseif isequal(correct_flat, 0)
        correct_flat = [];
    endif
endif
if !exist('refmapvers','var')
    refmapvers = "pag_";
endif
if !exist('normalize_background', 'var')
    normalize_background = [];
endif
if !isempty(normalize_background)
    if (length(normalize_background) == 4)
        bkg = round(normalize_background);
        printf('creating function normalize_background to normalize region [%d %d %d %d] to 1\n', bkg(1), bkg(2), bkg(3), bkg(4))
        normalize_background = @(x) x / mean(x(bkg(1):bkg(2), bkg(3):bkg(4))(:));
    elseif !is_function_handle(normalize_background)
        printf('normalize_background should be a function handle or a vector with region [vb vr hb he] to normalize to 1\n')
    endif
endif
if !exist('correct_ccd','var')
    correct_ccd = [];
endif
if !exist('correct_whitefield','var')
    correct_whitefield = [];
endif
switch correct_whitefield
    case 0
        correct_whitefield = [];
    case 1
        correct_whitefield = 'correct_whitefield_id16a';
        correct_whitefield_par = '/data/id16a/inhouse1/instrument/img1/whitefield/white.edf';
endswitch 
if !exist('correct_distortion','var')
    correct_distortion = [];
endif
# image_threshold, used to find zero images due to not working shutter
if !exist('image_threshold','var')
    image_threshold = 0;
endif
# image_positive, minimum in images, values below are cropped to minimum
if !exist('image_positive','var')
    image_positive = 1; # min value (integer) after dark subtraction
    #image_positive = [];
endif
if !exist('remove_spikes','var')
    remove_spikes = 0;
endif
if !exist('save_radiographs','var')
    save_radiographs = 0;
endif
if !exist('save_radiographs_slice','var')
    save_radiographs_slice = 0;
endif
if !exist('undo_log','var')
    undo_log = 0;
endif
if !exist('numbers','var')
    numbers = [];
endif
if !exist('vers','var')
    vers = '';
endif
if !exist('cylinder','var')
    cylinder = 0;
endif
if !exist('ext_lowfreq','var')
    ext_lowfreq = 0;
endif
if !exist('free_edge','var')
    free_edge = 0;
endif
if !exist('constrain','var')
    constrain = 0;
endif
if !exist('use_real_padding','var')
    use_real_padding = 1;
endif
if !exist('use_last_plane_for_real_padding')
    if (length(numbers) == 5)
        use_last_plane_for_real_padding = 1;
    else
        use_last_plane_for_real_padding = 0;
    endif
endif
if !exist("pad_images","var")
    pad_images = 1;
endif
if !exist("pad_method","var")
    pad_method = "reflect";
endif
if !exist("pad_extend_avg","var")
    pad_extend_avg = 20; # image is padded with average of pad_extend_avg last values
endif
if !exist("pad_consistent","var")
    pad_consistent = (use_real_padding >= 1);
endif
if !exist('pad_last_from_forward','var')
    pad_last_from_forward = 0;
endif
if !exist('pad_blend_last','var')
    pad_blend_last = [];
endif
if !exist('pad_blend','var')
    pad_blend.trans = -20;
    pad_blend.type = 'mix_lin';
    pad_blend.norm = 1;
    if pad_last_from_forward & isempty(pad_blend_last)
        # typically a broader transition is needed for the last distance when padded with forward calculation
        # pad_blend is not used for last distance otherwise
        pad_blend_last = pad_blend;
        pad_blend_last.trans = 200;
    endif
endif
if !exist('pixelsize_detector','var')
    pixelsize_detector = pixelsize;
    printf("!!! Use pixelsize_detector to indicate detector pixel size !!!")
endif
if !exist('correct_detector','var')
    correct_detector = 3;
endif
if !exist('oversamp','var')
    oversamp = 1;
endif
if !exist('reference_quali','var')
    reference_quali = 0;
endif
if !exist('reference_quali_debug','var')
    reference_quali_debug = 2;
endif
if !exist('reference_moved','var')
    reference_moved = 0;
endif
if !exist('reference_plane','var')
    reference_plane = 1;
endif
if !exist('recursive','var')
    recursive = 0;
endif
if !exist('recursive_purephase','var')
    recursive_purephase = 0;
endif
if !exist('recursive_unwrap','var')
    recursive_unwrap = 0;
endif
if !exist('recursive_n','var') 
    recursive_n = n; # optimized region vertically
endif
if !exist('recursive_m','var')
    recursive_m = m; # optimized region horizontally
endif
if !exist('recursive_save_iteration', 'var')
    recursive_save_iteration = 0;
endif
if !exist('recursive_mask_padded', 'var')
    recursive_mask_padded = (use_real_padding >= 1);
endif
if !exist('recursive_attenuate', 'var')
    recursive_attenuate = 0;
endif
if !exist('recursive_exclude', 'var')
    recursive_exclude = [];
endif
if !exist('recursive_mask', 'var')
    recursive_mask = 0;
endif
if !exist('recursive_constrain_grad', 'var')
    recursive_constrain_grad = 0;
endif
if !exist('recursive_rel_change', 'var')
    recursive_rel_change = 0;
endif
if !exist('recursive_reblur', 'var')
    recursive_reblur = 1;
endif
if !exist('recursive_scratch_remove', 'var')
    # remove initial phase map, after recursive reconstruction or not
    recursive_scratch_remove = 0;
endif
if !exist('two_pass', 'var')
    # use linear approach with blurring, followed by a recursive approach without blurring
    two_pass = 0;
endif
if !exist('resultdir_prepend','var')
    resultdir_prepend = '';
endif
if !exist('resultdir_append','var')
    resultdir_append = '';
endif
if !exist('save_projection','var')
    save_projection = 1;
endif
if !exist('save_absorption','var')
    save_absorption = 0;
endif
if !exist('show_projections','var')
    show_projections = interactive;
endif
if !exist('approach','var')
    approach = 4; # default approach is mixed approach
endif
if !exist('find_abs_plane','var')
    find_abs_plane = 1; # assume first plane is absorption
endif
if !exist('modified_delta','var')
    modified_delta  = 0;
endif
if !exist('lim1','var')
    switch approach
        case 8
            lim1 = 0
        otherwise
            lim1 = 1e-8
    endswitch
endif
if !exist('lim2','var')
    switch approach
        case 8
            lim2 = 0
        otherwise
            lim2 = 'gaussian_filter'
    endswitch
endif
if !exist('var_cut','var')
    var_cut = 1;
endif
if !exist('post_filter','var')
    post_filter = [];
endif
if !exist('post_crop_filter','var')
    post_crop_filter = [];
endif
if !exist("pre_filter","var")
    pre_filter = [];
end
if !exist("pre_filter_args","var")
    pre_filter_args = [];
end
if !exist('delta_beta','var')
    delta_beta = 0;
endif
if !exist('delta_beta_normalised','var')
    delta_beta_normalised = 0;
endif
if !exist('paganin_multiple_distance','var')
    paganin_multiple_distance = 1;
endif
if !exist('test_par_polyfit_order','var')
    test_par_polyfit_order = Inf;
endif
if !exist('Lcurve','var')
    Lcurve = 0;
endif
if !exist('object_prior', 'var')
    object_prior = 0;
endif
if !exist('preserve_offset','var')
    # from prior estimate (absorption) the offset is imposed
    # we should not modify it in order not to bias the RE
    if isequal(delta_beta, 0) && !object_prior
        preserve_offset = false;
    else
        preserve_offset = true;
    endif
endif
if !exist('calc_residu','var')
    calc_residu = 0; # estimate the residual error
endif
if ((approach == 4) | (approach == 7) | (approach == 8))
    if !exist('nloop_mixed','var')
        nloop_mixed = 3;
    endif
endif
if (approach == 5)
    if !exist('simple_approach','var')
        simple_approach = 0;
    endif
    if !exist('single_index','var')
        single_index = 0;
    endif
endif
if (approach == 7)
    if !exist('simple_approach7','var')
        simple_approach7 = -1;
    endif
endif
if !exist('ssigma','var')
    ssigma = 10;
endif
if !exist('strong_absorption','var')
    strong_absorption = 0;
endif
if !exist('subtr_filtered_mean','var')
    subtr_filtered_mean = 0;
endif
if !exist('do_total_mean','var')
    do_total_mean = 0;
endif
if !exist('centralpart','var')
    centralpart = 1;
endif
if !exist('centralpart_shift_v','var')
    centralpart_shift_v = 0;
endif
if !exist('centralpart_shift_h','var')
    centralpart_shift_h = 0;
endif
if !exist('magnification','var')
    magnification = 1;
endif
if !exist('magnification_large','var')
    magnification_large = 0;
endif
if !exist('axisposition','var')
    axisposition = 'global';
endif
if !exist('axisfilesduringscan','var')
    axisfilesduringscan = [];
endif
if isempty(axisfilesduringscan)
    if isempty(FT_AXISFILESDURINGSCAN)
        axisfilesduringscan = 1;
    else
        axisfilesduringscan = FT_AXISFILESDURINGSCAN;
    endif
endif
if !exist('correct_shrink','var')
    correct_shrink = 0;
endif
if !exist('correct_shrink_positive','var')
    correct_shrink_positive = 0;
endif
if !exist('use_shrink_list', 'var')
    use_shrink_list = 1;
endif
if !exist('shift_constant','var')
    shift_constant = 0;
endif
if !exist('shift_show','var')
    shift_show = interactive;
endif
if !exist('shift_show_contrast','var')
    shift_show_contrast = 1;
endif
if !exist('shift_approach','var')
    shift_approach = []; # findshift
elseif ( !interactive & ( shift_approach == 3 ) )
    printf('!!! shift_approach can not be 3 when job is not interactve !!!\n')
    printf('!!! changing shift_approach to default !!!\n')
    shift_approach = [];
endif
if isempty(shift_approach)
    shift_approach = 2; # default is real space correlation
endif
if ( (shift_approach == 7) & (shift == 1) )
    # use ctfs to determine the shifts
    shift_phased = 1;
else
    shift_phased = 0;
endif
if !exist('shift_useprevious','var')
    shift_useprevious = 1; # for shift_approach 2 and 3 
endif
if !exist('shift_estimate','var')
    shift_estimate = []; # for shift_approach 2 and 3 
endif
if !exist('shift_low_cut_off','var')
    shift_low_cut_off = 1; # for shift_approach 0; apply high pass filter (see correlate) 
endif
if !exist('shift_high_cut_off','var')
    shift_high_cut_off = 0; # for shift_approach 0; apply high pass filter (see correlate) 
endif
if !exist('shift_constrain','var')
    shift_constrain = 0;
endif
if !exist('shift_interactive','var')
    shift_interactive = 0;
endif
if !exist('shift_polyfit_order_h','var')
    shift_polyfit_order_h = [];
endif
if !exist('shift_polyfit_order_v','var')
    shift_polyfit_order_v = [];
endif
if !exist('shift_singleplot','var')
    shift_singleplot = 0;
endif
if !exist('shift_iterative','var')
    shift_iterative = false;
endif
if !exist('shift_iterative_save_indiv_mat')
    shift_iterative_save_indiv_mat = false;
else
    # shift_iterative_save_indiv_mat only relevant if shift_iterative is true
    shift_iterative_save_indiv_mat = shift_iterative_save_indiv_mat && shift_iterative;
endif
if !exist('shift_first_plane_zero','var')
    shift_first_plane_zero = 1;
endif
if !exist('shift_distrib','var')
    shift_distrib = 0;
endif
if !exist('random_disp','var')
   random_disp = 0; 
endif
if !exist('force_parfiles','var')
    force_parfiles = 0;
end 
if !exist('parfiles_preserve','var')
    parfiles_preserve = 0;
end 
if !exist('weight_threshold','var')
    weight_threshold = 0;   # threshold for weigthed approach 3
end     
if !exist('remove_vertical_background','var')
    remove_vertical_background = 0;     # post filter that removes a type of vertical background noise
end 
if !exist('normalize_to_median','var')
    if ( (approach == 1) & !strong_absorption & !object_prior)
        # images are normalized to 1 in this case
        normalize_to_median = 0;
    else
        # images are normalized to median
        normalize_to_median = 1;
    endif
endif


if isempty(projections)
    if shift
        # make sure nvue is included also when nvue is not a multiple of 100
        projections = unique([0:100:nvue nvue]);
    elseif !force_parfiles
        projections = 0;
    endif
endif
## Make sure projections is a row vector
projections = projections(:)';

########################################
#### parameter consistency checking ####
########################################
if !do_flatfield
    if !isempty(correct_ccd)
        warning("correct_ccd is set but will not be applied because do_flatfield = 0")
    endif
    if !isempty(correct_whitefield)
        warning("correct_whitefield is set but will not be applied because do_flatfield = 0")
    endif
    if !isempty(correct_distortion)
        warning("correct_distortion is set but will not be applied because do_flatfield = 0")
    endif
endif

#######################
##### info on scan ####
#######################

#####################
# filenames

nameprepend='_';
nameappend='_';

#################
# distances
# we allow for different curvatures of incident wavefront in vertical and horizontal direction
if !exist('z1v','var')
    z1v = [];
end
if isempty(z1v)
    z1v = z1;
end
if !exist('check_z1v','var')
    check_z1v = [];
end
if isempty(check_z1v)
    check_z1v = 0;
end
if !exist('check_spectrum','var')
    check_spectrum = [];
end
if isempty(check_spectrum)
    check_spectrum = 0;
end

if !exist('z1h','var')
    z1h = [];
end
if isempty(z1h)
    z1h = z1;
end

if !exist('z2h','var')
    z2h = [];
end
if isempty(z2h)
    z2h = z2;
end

if !exist('z2v','var')
    z2v = [];
end
if isempty(z2v)
    z2v = z2;
end

if !isempty(numbers)
    nb = length(numbers);
else
    nb = max([length(z2h) length(z2v) length(z1h) length(z1v)]);
end


# make cell array objects when relevant
pad_blend = convert_to_cell(pad_blend, nb);
if !isempty(pad_blend_last)
    pad_blend{nb} = pad_blend_last;
endif

## Make pos_feature non-obligatory parameter
if (!exist('pos_feature'))
  pos_feature = ones(nb,2);
  pos_feature(:,1) *= round(m/2);
  pos_feature(:,2) *= round(n/2);
end



#################
# wavelength

#################
# pixelsize

#################
# optics

#################################
##### info on reconstruction ####
#################################

# number of pixels (in reconstructed image)

if (length(centralpart) == 1)
    if centralpart == 1
        centralpart = [m/2 n/2];
    elseif centralpart == 0
        centralpart = [m n];
    else
        centralpart = [centralpart centralpart];
    endif
endif

if magnification
    if (rem(centralpart(1),2) | rem(centralpart(2),2))
        disp('Using centralpart and magnification is dangerous with centralpart(1)/2 or centralpart(2)/2 odd')
        disp('!!! Forcing centralpart = 0 !!!')
        centralpart = [m n];
    endif
endif

if (length(pad_images) == 2)
    nf = pad_images(1);
    mf = pad_images(2);       
elseif pad_images
    nf = 2^(ceil(log(n)/log(2))+floor(pad_images));
    mf = 2^(ceil(log(m)/log(2))+floor(pad_images));
    if (pad_images != floor(pad_images))
        nf *= 1.5;
        mf *= 1.5;
    endif
else
    nf = n;
    mf = m;
endif

params = struct('geo_params',struct('nf',nf,
                                    'mf', mf,
                                    'n',n,
                                    'm',m,
                                    'z1h',z1h ,
                                    'z2h',z2h ,
                                    'z1v',z1v ,
                                    'z2v',z2v ,
                                    'lambd', lambda,
                                    'pixelsize_detector', pixelsize_detector,
                                    'magnification', magnification,
                                    'magnification_large', magnification_large
                ),
                 'shift_params' , struct('shift',shift,
                                          'shift_approach',shift_approach,
                                          'shift_constant',shift_constant,
                                          'shift_first_plane_zero',shift_first_plane_zero,
                                          'shift_constant', shift_constant,
                                          'shift_phased',shift_phased 
                                          )
               );

save ("-hdf5", "params.h5", "-struct", "params")    
merge_structs = @(x,y) cell2struct([struct2cell(x);struct2cell(y)],[fieldnames(x);fieldnames(y)]);

if ((n!=nf) | (m!=mf))
    printf('Will pad images to %d (h) x %d (v)\n',mf,nf);
    if (shift & (shift_approach < 7))
        # fft based cross-correlation does not work well with other pad methods
        disp("Forcing pad_method to extend with fft based cross_correlation")
        pad_method = 'extend';
    endif
endif

params = struct('free_edge',free_edge);
save ("-hdf5", "unknown.h5", "-struct", "params")
                             
    
if !isequal(free_edge, 0)
    # find back edges in the padded image, will be used by find_phase
    hedge = round((mf-m)/2);
    free_edge = hedge + [1:free_edge m-free_edge+1:m];
endif

reco_parameters = pack_params;

###################
####  shifts ######
###################

###################
#### flatfield ####
###################

save_radiographs_append = 'flt';

###################
##### approach ####
###################

# only correction of shifts
# 0 no phase reconstruction

# only phase
# 1 pamsin

# phase + absorption
# 2 pamsin with absorption (absorption is slow envelope)
# 3 pamcossin (absorption is weak)
# 4 mixture pamsin / tie
# 5 calculate moments

###############################
# limits for 'wiener' filtering


old_params = load("params.h5" )
params = struct( 'filtering_params', struct('lim1',lim1,
                                     'lim2', lim2,
                                     'delta_beta', delta_beta,
				     'remove_spikes',remove_spikes
					   ));
new_params = merge_structs( params, old_params)
save ("-hdf5", "params.h5", "-struct", "new_params")

if !isnumeric(lim2)
    # use very small number for lim2 and apply post_filter instead
    post_filter = lim2;
    lim2 = 1e-30;
endif

# check if lim1 or lim2 are tested and make sure they have the same length
# do not accept lengths different from 1 that are not identical
if ( length(lim1) > 1 )
    lim_test = 1;
    if ( length(lim2) == 1 )
        lim2 = lim2*ones(size(lim1));
        printf("Testing lim1\n")
    elseif ( length(lim2) != length(lim1) )
        printf("lim1 and lim2 should have same length when vectors, exiting ...\n")
        return
    else
        printf("Testing lim1 and lim2\n")
    endif
elseif ( length(lim2) > 1 )
    lim_test = 2;
    lim1 = lim1*ones(size(lim2));
    # reorder lim2 to have largest value first (less noise, more blurred)
    lim2 = sort(lim2,'descend');
    printf("Testing lim2\n")   
else
    lim_test = 0;    
endif

###############################
# prior estimate
delta_beta_test = (length(delta_beta) > 1) ;
if delta_beta_test
    printf("Testing delta_beta\n")
endif



##### constrains #####

# create names of directories with individual scans
# standard is 
# /path/toto_1_
# /path/toto_2_ ...
# result directory will be /path/toto


old_params = load("unknown.h5" )
params = struct("numbers",numbers,"radio_only",radio_only, 'do_flatfield',do_flatfield , 'reference_quali', reference_quali , 'correct_shrink', correct_shrink,
 'reference_plane', reference_plane,'reference_moved',reference_moved,   
'subtr_filtered_mean',subtr_filtered_mean);
new_params = merge_structs( params, old_params)
save ("-hdf5", "unknown.h5", "-struct", "new_params")

#'




if !isempty(numbers)
    ### usual numbering scheme is used
    for k=1:nb
        direc{k} = strcat(namestr, nameprepend, num2str(numbers(k)), nameappend);
        prefix{k} = fullfile(direc{k}, direc{k});
        
    endfor
    resultdir = fullfile(dir, strcat(resultdir_prepend, namestr, nameprepend, resultdir_append));
    resultprefix = fullfile(resultdir, strcat(namestr, nameprepend, vers));
    
    printf( resultdir )
    printf( "\n")
    printf( resultprefix  )
    printf( "\n")

elseif !radio_only

    printf(" MI FERMO \n")
    disp( numbers)
    exit(0)

    vers_gen = 'ht_';
    ### case of single distance; complete name(s) is/are given in namestr
    if iscell(namestr)
        direc = namestr;
    else
        # case of a single distance
        direc{1} = namestr;
    endif

    # check that none of elements of direc end with '/' and create prefix
    for k = 1:nb
        if (direc{k}(end) == '/')
            direc{k} = direc{k}(1:end-1);
        endif
        prefix{k} = fullfile(direc{k}, direc{k});
    endfor
    # create name for result directory and result prefix
    resultdir = fullfile(dir, strcat(direc{1}, vers_gen));
    resultprefix = fullfile(resultdir, strcat(direc{1}, vers_gen, vers));
else # case of radiography
    exit(0)
    vers_gen = 'ht_';
    direc = namestr;
    prefix = fullfile(direc, direc);
    resultdir = fullfile(dir, strcat(direc, vers_gen));
    resultprefix = fullfile(resultdir,  strcat(direc, vers_gen, vers));
endif


printf(" DIREC================")
disp(direc)


[res, resultprefixshort] = fileparts(resultprefix);
if iscell(prefix)
    for k = 1:length(prefix)
        prefix{k} = fullfile(dir, prefix{k});
        direc{k} = fullfile(dir, direc{k});
    endfor
else
    prefix = fullfile(dir, prefix);
    direc = fullfile(dir, direc);
endif
refname = 'refHST';



#################
# check in batch mode if some projections need reconstruction
# this is for use of the besteffort queue
# ability to resume without recalculating everything
function_filename = evalin('caller', 'mfilename("fullpathext")')
if (!interactive)
    if (!shift)
        # find projections that don't exist yet
        projections = ht_oar_resume(projections, strcat(resultprefix, "%04d.edf"), ncrop*mcrop*4+512, function_filename);
        if isempty(projections)
            return;
        endif
    endif
endif



printf(" RESULT DIR \n")
disp(resultdir)

#################
# create result directory if not-existing
newdirectory = !exist(resultdir,'dir');
if newdirectory
    stat = mkdir(resultdir);
    if (exist ("OCTAVE_VERSION") != 5)
        stat = (stat == 0);
    endif
    if stat
        printf('New directory %s created successfully\n', resultdir)
        system(sprintf('chmod 770 %s',resultdir));
    else
        printf('Problems creating new directory %s, permissions ???\n',resultdir)
        return # EXITING PROGRAM !!!
    endif
endif

if !radio_only

    printf([ "DOING  # get information on individual scans\n"]);
    # get information on individual scans
    infofilename = [prefix{1} '.info'];
    printf("info\n")
    disp(infofilename)
    if exist(infofilename,"file")  # *.info exists
        fp = fopen(infofilename);
        hd = fscanf(fp,'%c');
        fclose(fp);
        scanrange = findheader(hd,'ScanRange','float');
    else    # *.info does not exist
        scanrange = [];  
    end
    if isempty(scanrange)
        scanrange = 180;
    endif
    is360 = (scanrange == 360);
    all_proj = [0:nvue+2+2*is360];
else
    printf("is360 0\n");
    is360 = 0;
endif
printf(" nvue\n")
disp(nvue)


# does tomo (dark.edf refHST) for all the individual scans if not yet done
if (do_flatfield && !radio_only)
    for k = 1:length(direc)
         printf([ " DOING # does tomo (dark.edf refHST) for all the individual scans if not yet done\n"]);
        if !exist(fullfile(direc{k}, ht_filename_image(refname, nvue)))
            printf("# (fast)tomo was not done, we do tomo (called without fileseparator at the end)\n")
            tomo(direc{k})
        endif
    endfor
endif

# apply quali90 (to see if sample moved during scan) to all the individual scans
if (shift & reference_quali)
    printf([ " DOING  # apply quali90 (to see if sample moved during scan) to all the individual scans\n"]);
    for k = 1:length(direc)
        if ( !isempty(correct_whitefield) & !isequal(correct_whitefield, 0) )
            quali_series(direc{k}, shift_approach, centralpart, 0, correct_shrink, 'auto', reference_quali_debug, random_disp,
                'shift_estimate', shift_estimate, 'low_cut_off', shift_low_cut_off, 'high_cut_off', shift_high_cut_off, 'pre_filter', correct_whitefield, 'pre_filter_args', correct_whitefield_par);            
        else
            quali_series(direc{k}, shift_approach, centralpart, 0, correct_shrink, 'auto', reference_quali_debug, random_disp,
                'shift_estimate', shift_estimate, 'low_cut_off', shift_low_cut_off, 'high_cut_off', shift_high_cut_off);
        endif
    endfor
endif

if magnification || magnification_large
    printf([ " DOING if magnification || magnification_large\n"]);
    Mh = (z1h+z2h)./z1h;
    Mv = (z1v+z2v)./z1v;
else
    Mh = Mv = ones(size(z1h));
endif

# evaluate results of quali_series
if (shift & reference_quali)
     printf([ " DOING if (shift & reference_quali)\n"]);
    legendstr = cell(nb, 1);
    rot_positions_all = corrh_all = corrv_all = [];
    for k = 1:length(direc)
        qres = load(fullfile(direc{k}, 'quali.mat'));
        rot_positions_all = [rot_positions_all qres.rot_positions'];
        corrh_all = [ corrh_all qres.corr_imagesafterscan(:,1)*pixelsize_detector/Mh(k)*1e6 ];
        corrv_all = [ corrv_all qres.corr_imagesafterscan(:,2)*pixelsize_detector/Mv(k)*1e6 ];
        legendstr{k} = sprintf("Plane %d", k);
    endfor
    figure(3)
    plot(rot_positions_all, abs(corrh_all))
    xlabel('Angle (degrees)')
    ylabel('Absolute value horizontal motion (microns)')
    title('Horizontal motion')
    legend(legendstr)
    drawnow
    figure(4)
    plot(rot_positions_all, corrv_all)
    xlabel('Angle (degrees)')
    ylabel('Vertical motion (microns)')
    title('Vertical motion')
    legend(legendstr)
    drawnow
    globalmotion = sum(abs(corrh_all), 1) + sum(abs(corrv_all), 1);
    printf("Sum absolute values movements:  %s\n", num2str(globalmotion))
    [minval, minpos] = min(globalmotion);
    printf("Suggested reference_plane: %d\n", minpos)
endif

#######################################################
# sample parameters

# characteristic lengthscale of the object to make problem dimensionless
le = 10e-6;
maxM = max([Mh Mv])
pixelsize = pixelsize_detector / maxM; # we bring everything to highest magnification
if pixelsize > 1e-6
    pixelsize_string = sprintf('%.1f um', pixelsize*1e6);
elseif pixelsize > 1e-7
    pixelsize_string = sprintf('%.3f um', pixelsize*1e6);
else
    pixelsize_string = sprintf('%.1f nm', pixelsize*1e9);
endif
if (magnification || magnification_large)
    printf('All images are resampled to smallest pixelsize: %s\n', pixelsize_string)
else
    printf('Pixelsize images: %s\n', pixelsize_string)
endif
# add pixel_size information in the header
# not very nice, but we do not want all other variables up to here 
reco_parameters.pixel_size = pixelsize;

# /path/toto/rhapp.mat is octave file containing variable rhapp with alignment errors between planes
# size is 2*nb*(nvue+3)
# first dimension: direction, vertical or horizontal
# second dimension: plane
# third dimension: number of the projection
# if rhapp.mat exists already it is read, otherwise rhapp starts from all zero, check that rhapp.mat exists when shift = 0 

name = fullfile(resultdir, 'rhapp.mat');
if (exist(name,'file') && (nb > 1))
     printf([ " DOING if (exist(name,'file') && (nb > 1))\n"]);

    load(name, 'rhapp')

    if size(rhapp, 2) != nb
        printf("Houston, we've had a problem here\n")
        if (size(rhapp, 2) < nb)
            disp("Size of shift file is not consistent with number of planes, returning...")
            return
        elseif (size(rhapp, 2) < max(numbers))
            printf("Largest value of numbers: %d was not included in shift file, returning...\n", max(numbers))
        else
            disp("Selecting limited numbers of planes from shift file")
            disp("Let's hope this works out")
            rhapp = rhapp(:, numbers, :);
        endif
    endif
elseif (shift || (nb == 1))
    if !radio_only
        rhapp = zeros(2,nb,nvue+3+2*is360);
    else
        rhapp = zeros(2,nb);
    endif
else
    printf("!!! File %s does not exist, run holotomo_master with shift = 1 at least once !!!\n",name)
    return     
endif

#namejumps = fullfile(resultdir, 'rhjumps.mat');
#if (exist(namejumps,'file'))
#    printf("Found file with jumps; we use it!\n")
#    load(namejumps, 'rhjumps')
#else
#    rhjumps = zeros(size(rhapp));
#endif

if ( random_disp )
    printf([ " DOING if ( random_disp )\n"]);
    # read random displacements
    printf("Reading all random displacements ...\n")
    disp( size(rhapp)) 
    rand_disp = zeros(2, nb, size(rhapp, 3));
    disp( size(rand_disp)) 


    # carefull, first index is ver, second is hor in rh/rhapp, opposite in correct.txt (use flip)
    # if rhapp has more angles than correct.txt, we keep zeros
    for k = 1:nb
        # read random displacement as stored by acquisition with checks
        c = ht_random_disp_read(direc{k}, 1e6*pixelsize*maxM./[Mh(k) Mv(k)])';
        rand_disp(:, k, 1:size(c,2)) = reshape(flip(c,1), 2, 1, size(c,2));
    endfor
    # correct for pixel size with respect to highest magnification
    rand_disp = rand_disp.*repmat(maxM./[Mv;Mh], [1 1 size(rand_disp, 3)]);

    if (rewrite_big)       
      stpar = struct( "rand_disp" , permute(rand_disp, [3,2,1] ));
      save ("-hdf5", "bigdata.h5", "-struct", "stpar") ;;
    endif            
endif


                             
if ( correct_shrink )
    shrinkv_all = shrinkh_all = zeros(nb, 1);
    for k = 1:nb
        if !use_shrink_list
            load(sprintf('shrink_%d_%d.mat', k, k+1));
            shrinkv_all(k) = shrinkv;
            shrinkh_all(k) = shrinkh;
        else
            load(fullfile(direc{k}, 'shrink_list.mat'));
            shrinkv_all(k) = shrink_list(1, 2);
            shrinkh_all(k) = shrink_list(1, 1);
            if exist('shrink_centerv', 'var')
                shrink_centerv_par(k) = shrink_centerv;
                shrink_centerh_par(k) = shrink_centerh;
                correct_shrink_centered = 0;
            else
                correct_shrink_centered = 1;
            endif
        endif
    endfor
    shrinkv_all = [ 0 ; cumsum(shrinkv_all) ]
    shrinkh_all = [ 0 ; cumsum(shrinkh_all) ]

    if correct_shrink_positive
        ## Do not allow negative shrinks
        printf("Forcing shrink values to be positive!\n")
        shrinkv_all = max(shrinkv_all,0);
        shrinkh_all = max(shrinkh_all,0);
    endif

endif

if not(shift)
					       printf([ " DOING if not(shift)\n"]);
    if ( interactive && !radio_only )
        # make sure no subplots remain
        figure(2);
        subplot(1, 1, 1);
        figure(1);
        subplot(1, 1, 1);
    endif
    if shift_constant
        # setting all alignment errors to the average over all projections
        # ?problem if not determined with all projections and fit was not acccepted?
        toto = mean(rhapp,3);
        for k = 1:size(rhapp,3)
            rhapp(:,:,k) = toto;
        endfor
    endif
    
    name = fullfile(resultdir, 'rhapp_iter.mat');
    if exist(name,'file')
        printf("Using %s for correcting shifts\n", name)
        load(name)
        rhapp += rhapp_iter;
    endif

    # we keep reference_plane fix
    if reference_plane
        if radio_only
            rhapp -= repmat(rhapp(:,reference_plane),1,nb);
        else
            for k = 1:size(rhapp,3)
                rhapp(1,:,k) -= rhapp(1,reference_plane,k); #+ rhjumps(1,reference_plane,k);
                rhapp(2,:,k) -= rhapp(2,reference_plane,k); #+ rhjumps(2,reference_plane,k);
            endfor
            if shift_first_plane_zero
                avg_plane_zero = mean(rhapp(:,1,:),3);
                rhapp -= repmat(avg_plane_zero, [1 nb size(rhapp,3)]);
            endif
        endif
    endif
    
    # correct for motion of 'reference_plane'
    if reference_moved
	 printf("	ECCO ", direc{reference_plane});
        [ref_h, ref_v] = correct_motion_calc(nvue, is360, direc{reference_plane},[],[],random_disp);
        for k = 1:size(rhapp,2)
            rhapp(:,k,:) += reshape([ref_v(1:size(rhapp,3))*maxM/Mv(reference_plane);ref_h(1:size(rhapp,3))*maxM/Mh(reference_plane)],2,1,size(rhapp,3));
        endfor
    endif
endif # not(shift)  

                             
                             
# correction large errors possible?
# only possible if reconstructed region is smaller than complete image
if !radio_only
    name = ht_filename_image(prefix{1}, 0);
else
    name = ht_filename_image(prefix, radiographs(1,1));
endif
hsize = header(name,'Dim_1','integer');
vsize = header(name,'Dim_2','integer');
if ( random_disp )
    ### random_disp compensate each distance for its random displacement
    ### take into account pixelsize
    rhapp(:,:,1:size(rand_disp,3)) -= rand_disp;
endif
                             
if shift
    rh = zeros(size(rhapp)); # rh will contain the shifts measured between the successive planes
else
    rh = rhapp; # rh will contain the shifts to be applied to the images
    if radio_only
        rh_mean = rh;
    else
        rh_mean = mean(rh, 3);
    endif
endif

# samplefrequency of the object
disp(le);
disp(pixelsize);

## prenderemo rhapp cosi come viene calcolato qua, troppe opzioni
if (rewrite_big)					       
     old_data = load("bigdata.h5" );
  datas =      struct( 'rh',permute(rh, [3,2,1]) )  ;
  new_datas = merge_structs( datas, old_data );
  save ("-hdf5", "bigdata.h5", "-struct", "new_datas")
endif
                             
old_params = load("params.h5" )
params = struct('length_scale',le, 'pixel_size', pixelsize, 'pixelsize_detector', pixelsize_detector, 'Mh',Mh,'Mv',Mv);
new_params = merge_structs( params, old_params)
save ("-hdf5", "params.h5", "-struct", "new_params")


fsamplex = le/pixelsize;
fsampley = le/pixelsize;

s = cell([1 nb]);
                             
if do_total_mean
    printf([ "DOING  if do_total_mean\n"]);
    smean = cell([1 nb]);
    disp("Will determine mean for each distance")
    disp("Forcing shift = 0, approach = 0, magnification_large = 0, magnification = 0, use_real_padding = 0")
    shift = 0;
    approach = 0;
    magnification_large = 0;
    magnification = 0;
    use_real_padding = 0;
endif

old_params = load("params.h5" )
params = struct( 'use_real_padding', use_real_padding )
new_params = merge_structs( params, old_params)
save ("-hdf5", "params.h5", "-struct", "new_params")

if ((use_real_padding > 0) && (use_last_plane_for_real_padding))
  exit(0)                             
  printf([ " DOING if ((use_real_padding > 0) && (use_last_plane_for_real_padding))\n"]);
    if !exist('planes', 'var')
        planes = [1:nb-1];
    endif

    # if shift has not been calculated for the fifth distance, add a zero shift for that
    if (size(rh,2) < nb)
        rh(:,nb,:) = 0;
        rhapp(:,nb,:) = 0;
    end
else
    if !exist('planes', 'var')
        planes = [1:nb];    # idea: select distances that are really used in reconstruction
    endif
end 

					       
if (!force_parfiles)
     if (approach & (not(shift) | shift_phased))
          printf([ " DOING     if (approach & (not(shift) | shift_phased))\n"]);
          #######################################################
          # argument
    
          # in reciprocal space, frequency coordinate
          #[f,g]=meshgrid(0:fsamplex/m:fsamplex/2,0:fsampley/n:fsampley/2);
         [ftot,gtot] = meshgrid([0:fsamplex/mf:fsamplex/2 -fsamplex/2+fsamplex/mf:fsamplex/mf:-fsamplex/mf],
				[0:fsampley/nf:fsampley/2 -fsampley/2+fsampley/nf:fsampley/nf:-fsampley/nf]);
					       
         [a,b] = meshgrid([0:2] ,[0:3]);

	 if (rewrite_big)					       
              old_data = load("bigdata.h5" );
	   datas =      struct( 'ftot', ftot'  ,'gtot',gtot','a',a,'b',b  )  ;
           new_datas = merge_structs( datas, old_data );
           save ("-hdf5", "bigdata.h5", "-struct", "new_datas")
	 endif  

        # fresnelnumbers and forward propagators
	distancesh = (z1h.*z2h)./(z1h+z2h);
        distancesv = (z1v.*z2v)./(z1v+z2v);

old_params = load("params.h5" )
params = struct( 'z1h',z1h  ,'z2h',z2h , "z1v",z1v,"z2v",z2v,'lambd',lambda )
new_params = merge_structs( params, old_params)
save ("-hdf5", "params.h5", "-struct", "new_params")



  
        betash = lambda*distancesh/le^2;
        betasv = lambda*distancesv/le^2;
        printf(" PASSO DI QUA  holotomo_slave.m\n");

                             
        # in case absorption image is required, count distance with respect to first plane 
        if ((approach == 4) | (approach == 5) | (approach == 7)) & (nb > 1)
            printf("!!! Measuring distance with respect to first plane !!!\n")
            betash -= betash(1);
            betasv -= betasv(1);
        endif
        # correction for shrinkage, not perfect!
        if correct_shrink
            # correction for shrinkage, not perfect!
            # correct for average during the scan
            disp("Approximate correction of propagation parameters for shrinkage")
            betash .*= (1+(shrinkh_all(1:end-1)+shrinkh_all(2:end))'/2).^2;
            betasv .*= (1+(shrinkv_all(1:end-1)+shrinkv_all(2:end))'/2).^2;
        endif
    
        if recursive
            ht_initialize_recursive
        endif
    
        #####################################################
        # correction for partial coherence (no factorisation)
        # every frequency component of the intensity will be multiplied by jo(lambda D f)
        switch correct_detector
            case 0
                disp('DOING Coherence envelope not considered')
                jo = 1;
            case 1
                disp('Single coherence envelope - with source size')
                # approximation to safe memory
                # take the same jo (distance 1) for all distances
                jo = mcf2(distancesh(1),le,ftot,gtot,[],oversamp,optic_used,1);
            case 2
                disp('DOING  Single coherence envelope - no source size')
                jo = mncf(nf,mf,oversamp,2,optic_used);
            case 3
                disp('DOING Calculating coherence envelope for all distances - no detector deconvolution')
                jo = cell([1 nb]);
                im = mncf(nf, mf, oversamp, 2, optic_used);
                for k = planes
                    if (k == 1)
                        jo{k} = ones(nf, mf);
                    else
                        jo{k} = mncf(nf, mf, oversamp*Mh(1)/Mh(k), 2, optic_used)./im;
                    endif
                endfor

                disp(nf)
                             disp(planes)
                disp(size(jo{1}))
                             
                if (rewrite_big)					       
		     old_data = load("bigdata.h5" );
		  datas =      struct( 'jo',jo{1}'  )  ;
		  new_datas = merge_structs( datas, old_data );
		  save ("-hdf5", "bigdata.h5", "-struct", "new_datas")
                endif

            case 4
                disp('DOING Calculating coherence envelope for all distances + detector deconvolution')
                jo = cell([1 nb]);
                for k = planes
                    jo{k} = mncf(nf, mf, oversamp*Mh(1)/Mh(k), 2, optic_used);
                endfor
        endswitch    

        if var_cut
	     printf([ " DOING         if var_cut\n"]);
             disp(var_cut)                
            if ( var_cut < 1 )
                cutn = var_cut;
            elseif ( var_cut == 1 )
                cutn = sqrt(1/2/betasv(end))/fsampley;
                cutn = min(cutn, 0.5);


                             
            elseif ( delta_beta(1) != 0 )
                     cutn = sqrt(1/pi/delta_beta(1)/betasv(end))/fsampley;
              cutn = min(cutn, 0.5);
            else
              cutn = sqrt(1/2/betasv(end))/fsampley;
              cutn = min(cutn, 0.5);
            endif
            printf("Normalized cut-off = %5.3f\n", cutn);
            r = filt2(nf,1+cutn*nf,0.01*nf,mf); # division low and high frequencies
	    if (rewrite_big)					       
		 old_data = load("bigdata.h5" );
              datas =      struct( 'r',r'  )  ;
              new_datas = merge_structs( datas, old_data );
              save ("-hdf5", "bigdata.h5", "-struct", "new_datas");
	    endif
	else
	  exit(0);
          cutn = 0.1;
          printf("Normalized cut-off = %5.3f\n", cutn);
	  r = filt2(nf,cutn*nf,0.01*nf,mf);
        endif
        lim = lim1(1).*r+lim2(1).*(1-r);

	if (rewrite_big)					       
             old_data = load("bigdata.h5" );
          datas =      struct( 'lim',lim'  )  ;
          new_datas = merge_structs( datas, old_data );
          save ("-hdf5", "bigdata.h5", "-struct", "new_datas");
	endif
                             
        if shift_phased
                             exit(0)
            cut_hf = 0.02;r_hf = 1-filt2(nf,cut_hf*nf,0.01*nf,mf);
            x = zeros(2*(length(planes)-1), 1);
        endif

        switch approach
            case {1,2} # pamsin and variants
                   ht_initialize_app1  ;
	      datas =      struct( 'ssq',ssq{1}'  )  ;
              save ("-hdf5", "ssq.h5", "-struct", "datas");
	      
		   
            case 3 # pamcossin
                ht_initialize_app3
            case 4 # mixed approach
                ht_initialize_app4
            case 5 # TIE
                ht_initialize_app5
            case 6 # PAM
                ht_initialize_app6
            case 7
                ht_initialize_app7
            case 8
                ht_initialize_app8
        endswitch
		printf([ " DOING ht_initialize_app1 FATTO\n"]);
        if !( ((approach == 8) && (!paganin_multiple_distance)) || delta_beta_test || lim_test )
                             printf("CLEAR GTOT\n" ) 
                             clear('gtot')
            if !strcmp(post_filter, 'sinogram_filter')
                             printf("CLEAR FTOT\n" ) 

                clear('ftot')
            endif
        endif


        if constrain # alternative
            printf(" CHIAMO ht_initialize constrain_n")
                             exit(0)
            ht_initialize_constrain
        endif # constrain

        if (pad_last_from_forward == 2)
            printf("pad_last_from_forward\n" )
                             exit(0)
            forward_mf = 2*mf;
            forward_ftot = [0:fsamplex/forward_mf:fsamplex/2 -fsamplex/2+fsamplex/forward_mf:fsamplex/forward_mf:-fsamplex/forward_mf];
            forward_prop = propagator2([betash(nb) ; betasv(nb)], forward_ftot, 0, 1){1};
            forward_r = filt2(forward_mf, 1+cutn*forward_mf, 0.01*forward_mf, 1)';
        endif
    endif # approach (not needed if no phase reconstruction)
    
    printf(" PASSO DI QUA\n")

    ##### Reading dark images of all scans #####
    dim_h = hsize;
    dim_v = vsize;

	    
    if (do_flatfield)
         printf([ " DOING     if (do_flatfield)\n"]);
      if radio_only
           printf(" radio only");
        exit(0);
        ## Read the dark
        name = fullfile(direc, 'dark.edf');
        im = edfread(name);
        if !isempty(correct_ccd)
		   ##disp('Correcting image')
		   im = feval(correct_ccd,im,correct_ccd_par);
        endif
          darks{1} = im';
        ##### Reading flatfield images of all distances #####
        flatnames = ht_infer_filenames(dir, strcat(namestr,"w"), radiographs);
        f1 = cell([1 nb]);
        f2 = cell([1 nb]);
        for k = 1:length(flatnames),
            name = flatnames{k};
          im = edfread(name);
          if !isempty(correct_ccd)
		     ##disp('Correcting image')
		     im = feval(correct_ccd,im,correct_ccd_par);
          endif   
            f1{k} = im'-darks{1};
          if !isempty(image_positive)
                     f1{k} = ht_image_positive(f1{k}, image_positive);
          endif
            if !isempty(correct_whitefield)
                       f1{k} = feval(correct_whitefield, transpose(f1{k}), correct_whitefield_par)';
            endif
            f2{k} = f1{k};
        endfor
          w1 = 1; w2 = 0;
      else
        darks = cell([1 nb]);
        for k = 1:nb
                    name = fullfile(direc{k}, 'dark.edf');
          im = edfread(name);
          printf(" DARK \n");
          disp(size(im));
          disp(nb);
          if !isempty(correct_ccd)
				#disp('Correcting image')
                     im = feval(correct_ccd,im,correct_ccd_par);
          endif
            darks{k} = im';
        endfor
	  
				#     @@@@@@@@@@@@ salvare dark
	  
##### Reading flatfield images of all scans #####
          refn1 = refon*floor(projections(1)/refon);
        refn2 = min(refn1+refon,nvue);
        f1 = cell([1 nb]);
        f2 = cell([1 nb]);
        refn1old = refn1;
        for k = 1:nb
                    name = fullfile(direc{k}, ht_filename_image(refname, refn1));
          printf(" REFFFFFNNNNAME \n");
          disp( name ) ;
          im = edfread(name);
          if !isempty(correct_ccd)
				#disp('Correcting image');
                     im = feval(correct_ccd,im,correct_ccd_par);
          endif   
            f1{k} = im'-darks{k};
          name = fullfile(direc{k}, ht_filename_image(refname, refn2));
          im = edfread(name);
          if !isempty(correct_ccd)
				#disp('Correcting image')
                     im = feval(correct_ccd,im,correct_ccd_par);
          endif   
            f2{k} = im'-darks{k};
          if !isempty(image_positive)
                     f1{k} = ht_image_positive(f1{k}, image_positive);
            f2{k} = ht_image_positive(f2{k}, image_positive);
          endif
            if !isempty(correct_whitefield)
                       f1{k} = feval(correct_whitefield, transpose(f1{k}), correct_whitefield_par)';
              f2{k} = feval(correct_whitefield, transpose(f2{k}), correct_whitefield_par)';
            endif
        endfor
      endif
    endif
      if (rewrite_big)					       
	   old_data = load("bigdata.h5" );
	tmp = zeros( size(f1{1}))' ;
	tmp(:,:) = f1{1}';
	tmp(:,:,2) = f2{1}';
	datas =      struct('flats_dark',
			    struct( 'dark',  darks{1}',
				    'fs', tmp ,
				    'refns', [refn1, refn2]'
				  )
			   );
	new_datas = merge_structs( datas, old_data );
	save ("-hdf5", "bigdata.h5", "-struct", "new_datas");
      endif
      
      if isscalar(subtr_filtered_mean)
        subtr_filtered_mean = subtr_filtered_mean*ones(1,nb);
      endif
      if (sum(subtr_filtered_mean))
           exit(0);
        printf([ " DOING     if (sum(subtr_filtered_mean))\n"]);
        for k = 1:nb
		    if subtr_filtered_mean(k)
					  name = fullfile(direc{k}, 'filtered_mean.edf');
                      if (subtr_filtered_mean(k) & exist(name,'file'))
			   filt_mean{k} = edfread(name)';
			if (subtr_filtered_mean(k) == 1)
                             printf('Filtered mean will be subtracted from all projections (log) for plane %d\n',k)
			elseif (subtr_filtered_mean(k) == 2)
				 printf('Filtered mean will be subtracted from all radiographs (no log) for plane %d\n',k)
			endif
                      else
			subtr_filtered_mean(k) = 0;
			printf('File %s is missing\n',k)
                      endif
		    endif
        endfor
      endif
      
      if !radio_only
	    printf([ " DOING     if !radio_only\n"]);
	disp(length(projections))
				# numero di radio richieste
            nloops = length(projections);
      else
        nloops = size(radiographs,1);
      endif
				# e finalmente qui si applica
    for q = 1:nloops
		printf([ " DOING     for q = 1:nloops  \n"]);
      disp(projections);
      tic
        if !radio_only
              v = projections(q);
	  printf(" vvvvvvvvvvvvvvvvv \n");
	  disp(projections);
	  
            # v est il numero reale ( quello che si pass in argomento se ce ne est uno solo )                            
        else
                             exit(0)
            v = 0;
            printf('Series of radiographs: %d\n',q)
        endif
        
        # update parameters that are angle dependent
        if exist('centralpart_shift_h_func')
                             exit(0)
            if is_function_handle(centralpart_shift_h_func)
                th = deg2rad(v/nvue*scanrange);
                centralpart_shift_h = min(max(round(centralpart_shift_h_func(th, centralpart_shift_h_par)), 1-m/2+centralpart(1)/2), 1+m/2-centralpart(1)/2)
            endif
        endif
	
      
	
	old_params = load("params.h5" );
      params = struct( 'miscellanea', struct(
					  'var_cut',var_cut ,
					  'planes', planes  , 
					  'correct_detector', correct_detector,
					  'correct_shrink', correct_shrink,
					  'approach',approach,
					  'recursive', recursive,
					  'subtr_filtered_mean',subtr_filtered_mean,
					  'centralpart', centralpart,
					  'centralpart_shift', [centralpart_shift_h centralpart_shift_v]),
		                          'remove_spikes',remove_spikes
		     );
      new_params = merge_structs( params, old_params);
      save ("-hdf5", "params.h5", "-struct", "new_params");
      
      if (do_flatfield & !radio_only)
				# updating eventually flatfield images
           refn1 = refon*floor(v/refon);
        refn2 = min(refn1+refon,nvue);
        printf(" ANCHE QUESTO? interpolazione \n")
            ## qui si avra la sequenza dei flat ( 2) e si interpola                 
            # salvare i flats                 
            refdiff = max((refn2-refn1),1);
            w1 = (refn2-v)/refdiff; 
            w2 = 1-w1;
    
            if refn1 != refn1old
                disp('update flatfields necessary')
                refn1old = refn1;
                f1 = f2;
                for k = 1:nb
                    name = fullfile(direc{k}, ht_filename_image(refname, refn2));
                    im = edfread(name);
                    if !isempty(correct_ccd)
                        #disp('Correcting image')
                        im = feval(correct_ccd,im,correct_ccd_par);
                    endif   
                    f2{k} = im'-darks{k};
                    if !isempty(image_positive)
                        f2{k} = ht_image_positive(f2{k}, image_positive);
                    endif
                    if !isempty(correct_whitefield)
                        f2{k} = feval(correct_whitefield, transpose(f2{k}), correct_whitefield_par)';
                    endif
                endfor
            endif
        endif # do_flatfield
    
        # reading images, cutting, resizing (magnification)
        for k = 1:nb
            # reading
		    ht_read_images;
	  printf([ " CALL             ht_read_images "  k  " \n"]);
	  
          disp(size(im));
          ## @@@@@@@@@@@@@@@@@@@@@ bisogna salvare im
	  
	  
				# old_data = load("images.h5" );
	  datas =     struct( 'im',  im' + darks{1}',
			      'np', projections(q),
			      'im_dark',  im'
			    );
	  
		       # new_datas = merge_structs( datas, old_data );
	  new_datas = datas ; 
	  save ("-hdf5", "images.h5", "-struct", "new_datas");
	  
	  
				# flatfield correction
# est qua dentro che c'e' il correlate  @@@@@@@@@@@@@@@@               
          ht_do_flatfield;
	  
				# remove spikes
          if remove_spikes>0
			     printf(" REMOVE SPIKES \n")
          # @@@@@@@@@@@@ si potra usare il ccd filtering probabilmente
			     new_datas = struct('im', im');
			     save ("-hdf5", "im_spikes_before_o.h5", "-struct", "new_datas");
			     
			     im = spikes(im,remove_spikes);

			     new_datas = struct('im', im');
			     save ("-hdf5", "im_spikes_o.h5", "-struct", "new_datas");
			      

          endif
        
            ## Image prefiltering. pre_filter is the name of octave
            ## function, and pre_filter_args a single argument or a cell
            ## array of arguments to be given to the filter. Note: For CTF
            ## a linear pre_filter should be equivalent to a post_filter
            if (!isempty(pre_filter))
                printf(" PREFILTER\n")
                             exit(0)
                im = feval_arg(pre_filter, im, pre_filter_args);
            endif
            
            if do_total_mean
                printf(" TOTAL MEAN\n")
                             exit(0)
                ht_total_mean
            endif
            # undo logarithm
            # + subtraction of filtered mean if relevant

            ## non fa niente                 
            ht_undo_logarithm;
            
            # correct distortions

            ## anche questa non fa niente                 
            ht_correct_distortions;
            
            # magnification
            ## @@@@@@@@@@@@@@ qui fa solo immagnifications                  ma non sembra fare niente
            ht_do_magnification;
	    printf(" USCITOA DA MAGNI\n");
        endfor # reading and correcting different planes
        
        clear('im')
        # Moving images (in case of real padding) and padding
        # This assumes smallest magnification is at k == nb
        # 
        for k=nb:-1:1
            # pad images
                             #@@@@@@@@@@@@ pad con reflect , interpolate on shift, cut away extra                 
            ht_do_padding;

	   
        endfor # Moving images (in case of real padding) and padding


	new_datas = struct('im', s{k}');
	save ("-hdf5", "dopo_o.h5", "-struct", "new_datas");

	
        if (use_real_padding == 2)
            keyboard
        end
        
        # Calculate mean for info and further normalization
        for k = planes
	  printf(["PPPPP piano "  " \n"])
	  disp( k ) 
	  disp( planes ) 
            sm(k) = mean2(s{k});
            #sm(k) = mean2(cut(s{k}, n, m)); # makes no significant difference if normalized during padding
        endfor
        disp("Average images");disp(sm)
        
        if (normalize_to_median == 1)
                             exit(0)
	  printf([ " DOING        if (normalize_to_median == 1)\n"]);
            sm_median = median(sm(planes));
            printf('Normalizing data to median: %g\n', sm_median)
            for k = planes
                s{k} /= (sm(k)/sm_median);
            endfor
            snorm = sm_median;
        else
            if ( (approach == 1) & !strong_absorption & !object_prior)
                # YES @@@@@@@@@@@@@@@
              disp('Normalizing data to 1');
	      disp( sm(k) ) 
                for k = planes
                    s{k} /= sm(k);
                endfor            
            endif
            snorm = mean(sm(planes));
        endif


        if ( magnification_large < 0 )
            return
        endif

	
        printf("  shift   shift_phased \n");
        disp( [   shift  ,  shift_phased   ]   ) 
        if ( shift & !shift_phased )
	  printf([ " DOING         if ( shift & !shift_phased )\n"]);
                             exit(0)

            for k = 2:nb
					       printf([ " DOING             for k = 2:nb" k " \n" ]);
                # determination of shifts
				if !is_valid_func(shift_estimate) & (shift_useprevious == 1)
					rhprevious = round(rhapp(:,k,v+1)-rhapp(:,k-1,v+1));
				else
					rhprevious = [0 ; 0];
				endif
                im0 = cut(s{k-1}, centralpart(2), centralpart(1), (nf+1)/2+centralpart_shift_v, (mf+1)/2+centralpart_shift_h);
                im1 = cut(s{k}, centralpart(2), centralpart(1), (nf+1)/2 + centralpart_shift_v - rhprevious(1), (mf+1)/2 + centralpart_shift_h - rhprevious(2));
                switch shift_approach
                    case 0  # compare images
                        if (shift_constrain)
                            rh(:,k,v+1) = rhprevious + correlate(im0, im1, 0,
                                shift_constrain_pos(:,k-1),
                                shift_constrain_size(:,k-1))';
                        else
                            rh(:,k,v+1) = rhprevious + correlate(im0, im1, 'low_cut_off', shift_low_cut_off, 'high_cut_off', shift_high_cut_off)'; 
                        endif
                    case 1  # compare (images-mean).^2
					       printf([ " DOING                     case 1  # compare (images-mean).^2\n"]);
                        ms = mean2(s{1});
                        if (shift_constrain)
                            rh(:,k,v+1) = rhprevious + correlate((im0-ms).^2,(im1-ms).^2,1,
                                shift_constrain_pos(:,k-1), shift_constrain_size(:,k-1))';
                        else
                            rh(:,k,v+1) = rhprevious + correlate((im0-ms).^2,(im1-ms).^2, 'low_cut_off', shift_low_cut_off, 'high_cut_off', shift_high_cut_off)';
                        endif
                    case 2
                        if is_valid_func(shift_estimate)
							rh(:,k,v+1) = rhprevious + findshift(im0, im1, [], shift_estimate)';
						else
							rh(:,k,v+1) = rhprevious + findshift(im0, im1)';
						endif
                    case 3
                        if is_valid_func(shift_estimate)
                            rhshiftestimate = round(findshift(im0, im1, 0, shift_estimate, 0))';
                            rhprevious += rhshiftestimate;
                            printf("Approximate shift from shift_estimate: Vertical: %f ; Horizontal %f\n", rhprevious(1), rhprevious(2))
							rh(:,k,v+1) = rhprevious + findshift(im0, circshift(im1, rhshiftestimate), 0, 'man')';
                        else
                            rh(:,k,v+1) = rhprevious + findshift(im0, im1, 0, 'man')';
                        endif
                    case 4
                        # no subpixel in normxcorr2 for the moment
                        # should use non-padded part of the images
                        [tmp,x0,y0] = normxcorr2(im0(2:end-1,2:end-1), im1);
                        tmp = [x0;y0]-[1;1];
                        rh(:,k,v+1) = rhprevious + tmp;
                    case 5  # call ij_turboreg
                        if is_valid_func(shift_estimate)
                            rhshiftestimate = round(findshift(im0, im1, 0, shift_estimate, 0))';
                            rhprevious += rhshiftestimate;
                            printf("Approximate shift from shift_estimate: Vertical: %f ; Horizontal %f\n", rhprevious(1), rhprevious(2))
							rh(:,k,v+1) = rhprevious + ij_turboreg(im0, circshift(im1, rhshiftestimate))';
                        else
                            rh(:,k,v+1) = rhprevious + ij_turboreg(im0, im1)';
                        endif
                        ijca
                    case 6
                        # elastix should come here
                endswitch # shift_approach
                printf('Vertical shift: %f ; Horizontal shift: %f\n', rh(1,k,v+1), rh(2,k,v+1))
            endfor
        elseif shift_phased
                             exit(0)
            switch shift_approach
                case 7
                    if (shift_useprevious == 1)
                        rhprevious = round(rhapp(:,:,v+1));
                    else
                        rhprevious = zeros(size(rhapp(:,:,1)));
                    endif

                    ht_shift_app7
                    
                    # absolute shifts
                    rhabs = rhprevious + cat(2, [0 0]',reshape(x*fact,[2 size(rhapp, 2)-1]));
                    # go back to relative shifts
                    rh(:, :, v+1) = cat(2, [0 0]', diff(rhabs, 1, 2));
            endswitch
        else # not(shift)
	       printf([ " DOING  --------------- >>>>>>>>>>>>>>>>>>>>>>        else # not(shift)\n"]);
            # moving images
            if check_spectrum
                             exit(0)
                cleantmp_imagej
                for k = planes
                    try
                        res = psd(s{k});
                        if !exist('psd_im', 'var')
                            # not nice
                            psd_im = zeros(size(res,1), nb);
                        endif
                        psd_im(:,k) += res(:,2);
                    end
                endfor
                semilogy(psd_im(1:ceil(size(psd_im,1)/2),:))
                drawnow
            elseif !do_total_mean
					       printf([ " DOING    >>>>>>>>>>>>>>>>>>>>>>>>>>         elseif !do_total_mean\n"]);
					disp( use_real_padding) 
                if (use_real_padding == 0) # case of regular padding, move images and do fft
                             exit(0)
                    for k=1:nb
                        s{k} = fft2(s{k});
                        if ((rh(:,k,v+1)'*rh(:,k,v+1)) > 0.001)
                            s{k} .*= translate(rh(1,k,v+1),rh(2,k,v+1), nf, mf);
                        endif
                    endfor
                else # case of real padding, moving images has been done already, do just the fft
                    for k=1:nb

                              filename = "provas.h5";
		      myslice = s{k} ;
		      
		      if (rewrite_big)
                           save("-hdf5", filename, "myslice")
		      endif
		      
		      new_datas = struct('data', s{k}');
		      save ("-hdf5", "im_mean_o.h5", "-struct", "new_datas");

                        ## @@@@@@@@@@@@ FACILE
                        printf(" FFT2 \n")
                        s{k} = fft2(s{k});


			new_datas = struct('data', real(s{k})');
			save ("-hdf5", "im_fft_o.h5", "-struct", "new_datas");

			
                    endfor
                endif
            endif
            ##########
            # saving radiographs
            if sum(save_radiographs)
                        exit(0)
                printf([ " DOING   >>>>>>>>>>>>>>>>>>          if sum(save_radiographs)\n"]);
                if isscalar(save_radiographs)
                    save_radiographs = zeros(1,nb);
                    save_radiographs(planes) = 1;
                endif
                for k = find(save_radiographs)
                    if !radio_only
                        name = ht_filename_image(strcat(prefix{k}, save_radiographs_append), v);
                    else
                        name = sprintf('%s%s_%d_.edf', prefix, save_radiographs_append, k);
                    endif
                    proj = real(ifft2(s{k}));
                    if ((ncrop != nf) | (mcrop != mf))
                        proj = cut(proj, ncrop, mcrop);
                    endif
                    if (save_radiographs_slice == 0)
                        edfwrite(name,proj','float32')
                        printf('Radiograph %d in plane %d saved as %s\n',v,k,name);
                    elseif (save_radiographs_slice <= n)
                        edfwrite(name,proj(save_radiographs_slice,:)','float32')
                        printf('Radiograph %d in plane %d saved as %s\n',v,k,name);
                    else
                        printf('Slice %d is out of range\n',save_radiograph_slice);
                    endif
                endfor # (different planes)
            endif
            # phase retrieval
            switch approach
                case 1 # CTF without absorption; assumes homogeneous object when delta_beta is given
                    printf([ " CALL ht_app1\n"]);


                    ht_app1


                    # constrain         
                    switch constrain
                      case 0
			printf(" find_phase\n");
			disp( [ext_lowfreq, free_edge, preserve_offset]) ;

                        ph = find_phase(ph, ext_lowfreq, free_edge, preserve_offset);
			disp(size(ph) );

                        if object_prior
                             ph ./= sa;
                        endif

                      case 1 # 1 sign phase e.g. foam
                             ph0 = real(ifft2(ph.*(1-rconst)));
                        index = 1;
                        d = zeros(indexmax,1);
                        for k = 1:pos-1
					for l=1:pos-1
                        if (rconst(k,l)>0.01)
                        d(index)=real(firec0(k,l));
                        index=index+1;
                        if any([k-1 l-1])
                            d(index)=imag(firec0(k,l));
                            index=index+1;
                        endif
                        if (l>1)&(k>1)
                        d(index)=real(firec0(k,mf-l+2));
                        index=index+1;
                        d(index)=imag(firec0(k,mf-l+2));
                        index=index+1;
                        endif
                        endif
                        endfor
                        endfor
                        m4 = lsqlin(c,d,(-sign(constrainflag))*a,sign(constrainflag)*ph0(:),[],[],[],[],m4,options('TolFun',1e-6));
                        ph = a*m4;  
                        ph = reshape(ph,nf,mf)+ph0;
                        case 2 # 2 minimum background e.g. isolated particles
                        ph0 = real(ifft2(ph.*(1-rconst)));
                            disp('Not yet correctly implemented')
                        return # not yet correctly implemented: selection of p (= 0 where no objects, = 1 where objects)
                        p = (X.^2+Y.^2)<0.01;
                        tt = find(p);
                        ttn = find(1-p);
                        A = zeros(size(a,2));
                        for k = 1:size(a,2)
                        for l=1:size(a,2) 
                        if ( l<k )
                        A(k,l) = A(l,k);
                        else
                        A(k,l) = ((k==l) - sum(a(tt,k).*a(tt,l)));
                        endif
                        endfor
                        endfor
                        C = ph0(ttn)'*a(ttn,:);
                        B = A\C';
                        ph = ph0-reshape(a*B,nf,mf);
                    endswitch # constrain
                    # recursive part
                    if recursive
		      exit(0);
                        ###########
                        # mal2
                        disp('Starting mal loop')
                        if (recursive_scratch == 1)
                            disp('Forcing initial phase to zero')
                            ph *= 0;
                        elseif ischar(recursive_scratch)
                            fname = ht_filename_image(recursive_scratch, v);
                            printf("Reading initial phase from %s\n", fname)
                            ph = edfread(fname)';
                            if ( ( size(ph,1) != nf ) | ( size(ph,2) != mf ) )
                                ph = im_pad(ph, mf, nf, pad_method, pad_extend_avg);
                            endif
                        endif
                        if strong_absorption
                            trec = sqrt(im).*exp(i*ph);
                        elseif object_prior
                            trec = sqrt(sa).*exp(i*ph);
                        elseif delta_beta
                            trec = sqrt(snorm)*exp((1/delta_beta+i)*ph);
                        else
                            trec = sqrt(snorm)*exp(i*ph);
                        endif
                        im = abs(trec);
                        trec = fft2(trec);
                        for k=1:nb
                            if strong_absorption
                                s{k} = im.*real(ifft2(s{k}));
                            elseif object_prior
                                s{k} = sa + real(ifft2(s{k}));
                            else
                                s{k} = snorm*real(ifft2(s{k}));
                            endif
                        endfor
    
                        ht_recursive
                        
                        trec = ifft2(trec);
                        im = abs(trec); # amplitude
                        p = angle(trec); # wrapped phase
                        ph = p+2*pi*round((ph-p)/2/pi); # phase unwrapping, \
                                    # difference with initial estimate of the phase smaller than 2*pi
                    endif # recursive
                case 2 # approach = 2 #  CTF with division by absorption image (first one)
                    ht_app2     
                case 3 # approach = 3 # CTF with phase and absorption
                    ht_app3
                    switch constrain
                        case 0
                            ph = find_phase(ph,ext_lowfreq,free_edge,preserve_offset*(delta_beta_val!=0));
                            if save_absorption
                                im = real(ifft2(im));
                            endif
                        case 1 # 1 sign phase e.g. foam
                            firec0 = kaa.*firecp-kap.*fireca;
                            ph0 = real(ifft2(ph.*(1-rconst)));
                            index = 1;
                            d = zeros(indexmax,1);
                            for k=1:pos-1
                                for l=1:pos-1
                                if (rconst(k,l)>0.01)
                                d(index) = real(firec0(k,l));
                                index++;
                                if any([k-1 l-1])
                                d(index) = imag(firec0(k,l));
                                index++;
                                endif
                                if (l>1)&(k>1)
                                d(index) = real(firec0(k,mf-l+2));
                                index++;
                                d(index) = imag(firec0(k,mf-l+2));
                                index++;
                                endif
                                endif
                                endfor
                            endfor
                            m4 = lsqlin(c,d,(-sign(constrainflag))*a,sign(constrainflag)*ph0(:),[],[],[],[],m4);
                            ph = a*m4;  
                            ph = reshape(ph,nf,mf)+ph0;
                            im = real(ifft2(im));
                        case 2 # 2 minimum background e.g. isolated particles
                            ph0 = real(ifft2(ph.*(1-rconst)));
                            p = findobject(ph0);
                            tt = find(p);
                            ttn = find(1-p);
                            A = zeros(size(a,2));
                            for k = 1:size(a,2)
                            for l = 1:size(a,2) 
                            if l<k
                            A(k,l) = A(l,k);
                            else
                            A(k,l) = ((k==l) - sum(a(tt,k).*a(tt,l)));
                            endif
                            endfor
                            endfor
                            C = ph0(ttn)'*a(ttn,:);
                            B = A\C';
                            phl = reshape(a*B,nf,mf);
                            ph = ph0-phl;
                    endswitch # constrain
                case 4 # approach = 4 #  mixed tie / pamsin
                    ht_app4
                case 5 # approach = 5 # TIE
                    ht_app5
                case 6 # approach = 6 # PAM (Paraboloid method)
                    ht_app6
                    if recursive
                        ###########
                        # mal2
                        disp('Starting mal loop')
                        if recursive_purephase
                            trec = ht_purephase(ph, snorm);
                        else
                            trec = ph;
                        endif
                        ht_recursive
                        trec = ifft2(trec);
                        im = abs(trec); # amplitude
                        ph = angle(trec); # wrapped phase
                    endif # recursive
                case 7 # approach 7 # symmetric mixed
                    ht_app7
                case 8 # approach 8 # homogeneous object, Paganin with multiple distances
                    ht_app8
                    # recursive part
                    if recursive
                        ###########
                        # mal2
                        disp('Starting mal loop')
                        if (recursive_scratch == 1)
                            disp('Forcing initial phase to zero')
                        elseif ischar(recursive_scratch)
                            fname = ht_filename_image(recursive_scratch, v);
                            printf("Reading initial phase from %s\n", fname)
                            ph = edfread(fname)';
                            if ( ( size(ph,1) != nf ) | ( size(ph,2) != mf ) )
                                ph = im_pad(ph, mf, nf, pad_method, pad_extend_avg);
                            endif
                        endif
                        # set only phase to zero
                        trec = exp(1/delta_beta*ph + (i*(!isequal(recursive_scratch,1)))*ph);
                        if (recursive_purephase == 5)
                            im = abs(trec);
                        endif
                        trec = fft2(trec);
                        for k=1:nb
                            s{k} = real(ifft2(s{k}));
                        endfor
    
                        ht_recursive
                        trec = ifft2(trec);
                        im = abs(trec); # amplitude
                        p = angle(trec); # wrapped phase
                        ph = p+2*pi*round((ph-p)/2/pi); # phase unwrapping, \
                                    # difference with initial estimate of the phase smaller than 2*pi
                    endif # recursive
            endswitch # approach
            ##########
            # saving phase map ...
            if approach
                # apply post filter if relevant
              if !isempty(post_filter)
		exit(0);
                    if strcmp(post_filter, 'sinogram_filter')
                        # 1D fft should be sufficient, but this gives segmentation fault for large matrices ???
                        #ph = 2*real(ifft2(fft2(ph).*abs(ftot)/fsamplex.*cos((pi/fsamplex)*ftot)));
                        ph = 2*real(ifft2(fft2(ph).*abs(ftot)/fsamplex));
                        # 2* is empiric, but needed to get the same result with PyHST
                        # is this mf/m ?
                    else
                        ph = feval(post_filter, ph);
                    endif
                endif
                # crop images to dimensions ncrop x mcrop
                if ((ncrop != nf) | (mcrop != mf))
                    if ncrop_consistent
                        # crop centered on last distance
                        cncrop = (1 + nf)/2 + rh_mean(1, end);
                        printf("Center vertical cropping on %f\n", cncrop) 
                    else
                        # crop centered on padded image
                        cncrop = [];
                    endif
                    if mcrop_consistent
                        # crop centered on last distance
                        cmcrop = (1 + mf)/2 + rh_mean(2, end); 
                        printf("Center horizontal cropping on %f\n", cmcrop) 
                    else
                        # crop centered on padded image
                        cmcrop = [];
                    endif
		    disp( [ncrop, cncrop]) 
                    ph = cut(ph, ncrop, mcrop, cncrop, cmcrop);
		    
		    exit(0)
                endif
                if !isempty(post_crop_filter)
		  exit(0)
                    ph = feval(post_crop_filter, ph);
                endif
                # save phase map
                if save_projection
                    #idea: have everything up to date, but maybe too many variables and issue with complex variables
                    #reco_parameters = pack_params;
                    ##save -binary reco_parameters.mat reco_parameters
                    name = ht_filename_image(resultprefix, v);
                    edfwrite(name, ph', 'float32', reco_parameters)
                    printf('Phase Map %d saved as %s\n',v,name);
                    
                    #idea: save reco_parameters as h5-file
                    #issues with strings in saved file, they appear as individual integers
                    #name_h5 = ht_filename_image(resultprefix, v, 'h5');
                    #save("-hdf5", name_h5, 'reco_parameters')
                    #printf('Reconstruction parameters saved as %s\n', name_h5);
                    
                    if (show_projections & (length(projections) == 1))
                        if isempty(IMAGEJ_STARTED)
                            init_ij;
                        endif
                        ij_open(name);
                    endif
                else
                    printf('Phase Map %d done, but not saved!\n',v);
                end
                # save absorption
                if ( save_absorption )
                    # go back to original dimensions
                    if ((n!=nf) | (m!=mf))
                        im = cut(im, n, m);
                    endif
                    name = ht_filename_image(strcat(resultprefix, 'abs'), v);
                    edfwrite(name, im', 'float32')
                    printf('Attenuation Map %d saved as %s\n',v,name);
                    if (show_projections & (length(projections) == 1))
                        ij_open(name);
                    endif
		      exit(0);
                endif
                
                # clean intermediate data to save disk space
                if (recursive & two_pass & recursive_scratch_remove)
                    fname = ht_filename_image(recursive_scratch, v);
                    printf("Removing initial phase map %s\n", fname)
                    unlink(fname);
                endif
            endif
        endif # not(shift)
        printf('Time : %f\n',toc)
    endfor # main loop
endif # !force_parfiles

# Things to be done after main loop
if shift
    if shift_show
        if isempty(IMAGEJ_STARTED)
            init_ij;
        else
            ij_closeall
        endif        
        rht = cumsum(rh,2);     
        for k=1:nb
            if (rht(:,k,v+1)'*rht(:,k,v+1))>0.01
                if isequal(centralpart, [m n])
                    s{k} = fft2(s{k}).*translate(rht(1,k,v+1),rht(2,k,v+1),nf, mf);
                else
                    s{k} = fft2(s{k}).*translate(rht(1,k,v+1),rht(2,k,v+1),nf, mf);
                endif
                s{k} = real(ifft2(s{k}));
            endif
            if ((n!=nf) | (m!=mf))
                s{k} = cut(s{k}, centralpart(2), centralpart(1), (nf+1)/2+centralpart_shift_v, (mf+1)/2+centralpart_shift_h);
            endif
            if !radio_only
                name = sprintf('%s%4.4i_%d.edf', resultprefix, v, k);
            else
                name = sprintf('%s%4.4i_%d.edf', resultprefix, q, k);
            endif
            edfwrite(name, transpose(s{k}), "float32");
            printf('Image %d saved as %s\n', k, name);
            if shift_show_contrast
                ij_open(name); # should this be before the switch ?
                ij_contrast;
            else
                ij(transpose(s{k}))
            endif
            ij_rename(sprintf('Distance %d',k));
        endfor
        if (nb > 1)
            ij_converttostack
        endif
    endif # shift_show
    # fit of displacements
    if ( random_disp )
        # remove motion due to random displacement to make fitting smooth
        rh(:,2:nb,projections+1) += diff(rand_disp(:,:,projections+1),1,2);  
    endif
    # call ht_fitshifts for polynomial fitting of shifts and creation of rhapp.mat and rhappnofit.mat
    if !shift_distrib
        ht_fitshifts(resultdir, shift_interactive, rh, projections,
            [shift_polyfit_order_h shift_polyfit_order_v], shift_singleplot);
    else
        fname = 'rhapp_distrib_%d_%d.mat';
        projections_fit = projections;
        save(sprintf(fname, projections(1), projections(end)), 'rh', 'projections_fit')
        printf("Saving %s ...\n", fname)
    endif
    
    # check for vertical distortion
    if sum(check_z1v>0)
        if ( check_z1v == 2 )
            for k = 1:nb-1
                [corv shrinkv corh shrinkh] = deform_optic2(s{k}, s{k+1});
                save(sprintf('shrink_%d_%d.mat', k, k+1),
                    'corv', 'shrinkv', 'corh', 'shrinkh')
            endfor
        else
            figure(3);
            if (length(check_z1v) == 2)
                check_z1v_1 = check_z1v(1);
                check_z1v_2 = check_z1v(2);
            else
                check_z1v_1 = nb-1;
                check_z1v_2 = nb;
            endif
            printf("Using plane %d and %d for checking z1v\n",check_z1v_1,check_z1v_2);
            [yc,shrink] = deform_vert_rs(s{check_z1v_1},s{check_z1v_2});
            title('Residual distortion')
            xlabel('Subpart')
            ylabel('Pixels')
            z1v_calc = -(z2v(check_z1v_2)-z2v(check_z1v_1))/shrink;
            if magnification
                z1v_calc = 1/(1/z1v_calc + 1/z1v);
            endif
            printf("Estimated value of z1: %5.2f\n",z1v_calc)
            printf("To be used with magnification = 1\n")
        endif
    endif
else # not shift
    if strcmp(post_filter, 'sinogram_filter')
        no_sinogram_filtering = 1;
    else
        no_sinogram_filtering = [];
    endif
    if !radio_only
       if axisfilesduringscan
            projection_needed = ceil(180/scanrange*nvue);
        else
            projection_needed = nvue+2+2*is360;
        endif
        # check if we can create par-files etc
        if ((find(projections==projection_needed) && save_projection) || force_parfiles)
            if axisfilesduringscan
                printf("Using files during scan for axis determination\n")
                index_0 = 0;
                index_180 = index_0 + 180/(scanrange/nvue);
            else
                printf("Using files at end of scan for axis determination\n")
                if is360
                    index_0 = nvue+4;
                    index_180 = nvue+2;
                else
                    index_0 = nvue+2;
                    index_180 = nvue;
                endif
            endif
            if ((round(index_180)-index_180) < 1e-6)
                index_180 = round(index_180);
            endif
            try 
                ph = create_projection(resultdir,resultprefixshort,index_0)';
                im = create_projection(resultdir,resultprefixshort,index_180)';
            catch
                ph = [];
                im = [];
            end_try_catch
            if ((length(ph)>1) && (length(im)>1))
                # parameters from original par file
                origparfilename = [prefix{reference_plane} '.par'];
                if ( exist(origparfilename) && parfiles_preserve )
                    #read subvolume from original scan
                    disp('Reading reconstruction parameters ...')
                    fid = fopen(origparfilename);
                    hd =fscanf(fid,'%c');
                    start_voxel_1 = max(findheader(hd,'START_VOXEL_1','integer'), 1);
                    start_voxel_2 = max(findheader(hd,'START_VOXEL_2','integer'), 1);
                    start_voxel_3 = max(findheader(hd,'START_VOXEL_3','integer'), 1);
                    sizex = min(findheader(hd,'END_VOXEL_1','integer'), mcrop)-start_voxel_1+1;
                    sizey = min(findheader(hd,'END_VOXEL_2','integer'), mcrop)-start_voxel_2+1;
                    sizez = min(findheader(hd,'END_VOXEL_3','integer'), ncrop)-start_voxel_3+1;
                    offangle = findheader(hd,'ANGLE_OFFSET','float');
                    offset_refplane = findheader(hd,'ROTATION_AXIS_POSITION','float') - m/2;
                    fclose(fid);
                else
                    disp('Selecting total volume ...')
                    start_voxel_1 = 1;
                    start_voxel_2 = 1;
                    start_voxel_3 = 1;
                    sizex = mcrop;
                    sizey = mcrop;
                    sizez = ncrop;
                    offangle = 0;
                    offset_refplane = [];
                endif
                # rotation axis determination
                printf('Determination rotation axis ...\n')
                switch axisposition
                    case 'global'
                        lim = 1-filt2(ncrop, 0.05*ncrop, 0.01*ncrop, mcrop);
                        c = correlate(fft2(ph).*lim,fft2(fliplr(im)).*lim,1);
                    case 'accurate'
                        c = findshift(cut(ph, ncrop/2, mcrop/2), fliplr(cut(im, ncrop/2, mcrop/2)));
                    case 'manual'
                        c = fliplr(ij_align(ph',rot90(im)));
                endswitch
                if (abs(c(1)) > 0.5)
                    printf('Vertical motion would be %3.2f pixels\n',c(1))
                endif
                offset = c(2)/2;
        # attempt to check rotation axis calculation with help of result for individual scan
        # problem: take correctly into account the magnification
        #         offset_tolerance = 1;
        #         if isempty(offset_refplane)
        #             printf('%s does not exist !!!\n',origparfilename)
        #             printf('We hope rotation axis from phase maps is correct\n')
        #         elseif (abs(offset-offset_refplane) < offset_tolerance)
        #             printf('Phase maps and reference plane are consistent within %g pixels\n',offset_tolerance)
        #             printf('We use rotation axis from phase maps\n')
        #         else    
        #             printf('Phase maps and reference plane are not consistent\n')
        #             printf('Phase maps offset: %3.2f pixels\n',offset)
        #             printf('Reference plane offset: %3.2f pixels\n',offset_refplane)
        #             printf('We use rotation axis from reference plane\n')
        #             offset = offset_refplane;
        #         endif
                printf('Offset rotation axis is %3.2f pixels\n',offset)

                # simple info file
                infofilename = [resultprefix '.info'];
                printf('Writing %s\n',infofilename)
                infofile(infofilename,'Prefix=',resultprefixshort,'Directory=',resultdir,'TOMO_N=',nvue,'Dim_1=',mcrop,'Dim_2=',ncrop,
                    'Date=',asctime(localtime(time)),'Optic_used=',pixelsize*1e6,
                    'pad_method=',pad_method,'pad_images=',pad_images,'pad_extend_avg=',pad_extend_avg,'nb_of_planes=',nb,
                    'magnification=',magnification,'correct_detector=',correct_detector,'approach=',approach,
                    'lim1=',lim1,'lim2=',lim2,'post_filter=',post_filter,'delta_beta=',delta_beta,
                    'ssigma=',ssigma,'cylinder=',cylinder,
                    'ext_lowfreq=',ext_lowfreq,'free_edge=',free_edge,'constrain=',constrain,'recursive=',recursive);

                # save packed HT parameters
                parameterfilename = [resultprefix '.mat'];
                parameters_ht = pack_params;
                printf('Writing %s\n', parameterfilename)
                save('-binary', parameterfilename, 'parameters_ht')
                clear('parameters_ht')

                # parfile for single slice
                pyhst_parfile(resultdir,resultprefixshort,nvue,mcrop,ncrop,pixelsize*1e6,'NO',[],offset,
                    'start_voxel_1',1,'end_voxel_1',mcrop,
                    'start_voxel_2',1,'end_voxel_2',mcrop,
                    'start_voxel_3',round(ncrop/2),'end_voxel_3',round(ncrop/2),
                    'parfilename',[resultprefixshort 'slice.par'],'angle_between_projections',-scanrange/nvue,
                    'no_sinogram_filtering', no_sinogram_filtering);
                disp('Created file for tomographic reconstruction single slice')

                # parfile for volume
                create_vol_dirs(dir)
                floatname = 'volfloat';
                volname = fullfile(dir, floatname, [resultprefixshort '.vol']);
                pyhst_parfile(resultdir,resultprefixshort,nvue,mcrop,ncrop,pixelsize*1e6,'NO',[],offset,
                    'start_voxel_1',start_voxel_1,'end_voxel_1',start_voxel_1+sizex-1,
                    'start_voxel_2',start_voxel_2,'end_voxel_2',start_voxel_2+sizey-1,
                    'start_voxel_3',start_voxel_3,'end_voxel_3',start_voxel_3+sizez-1,
                    'output_file',volname,
                    'angle_offset',offangle,
                    'angle_between_projections',-scanrange/nvue,
                    'no_sinogram_filtering', no_sinogram_filtering);
                disp('Created file for tomographic reconstruction volume')

                # parfile for testing of HT parameters
                if ( (approach == 1) | (approach == 8) )
                    if (((delta_beta_test | lim_test) & !Lcurve) & ((length(projections) > 1) | (force_parfiles)))
                        # parameter files for testing of parameter (lim1/2, delta_beta)
                        for k = 1:loopval
                            resultdir_param = fullfile(resultdir, sprintf('param_%02d_', k));
                            pyhst_parfile(resultdir_param,resultprefixshort,nvue,m,1,pixelsize*1e6,'NO',[],offset,
                                'start_voxel_1',1,'end_voxel_1',m,
                                'start_voxel_2',1,'end_voxel_2',m,
                                'start_voxel_3',1,'end_voxel_3',1,
                                'parfilename',[resultprefixshort 'slice.par'],
                                'angle_between_projections',-scanrange/nvue,
                                'no_sinogram_filtering', no_sinogram_filtering);
                            disp('Created file for testing parameter on single slice')
                        endfor
                    endif
                endif

                if (recursive_save_iteration)
                    # parameter files for intermediate iterations
                    for k = 1:nloopm
                        resultdir_param = fullfile(resultdir, sprintf('iteration_%d_', k));
                        pyhst_parfile(resultdir_param,resultprefixshort,nvue,m,1,pixelsize*1e6,'NO',[],offset,
                            'start_voxel_1',1,'end_voxel_1',m,
                            'start_voxel_2',1,'end_voxel_2',m,
                            'start_voxel_3',1,'end_voxel_3',1,
                            'parfilename',[resultprefixshort 'slice.par'],
                            'angle_between_projections',-scanrange/nvue,
                            'no_sinogram_filtering', no_sinogram_filtering);
                        disp('Created file for intermediate iteration on single slice')
                    endfor
                endif
            else
                disp('Impossible to create .par file for tomographic reconstruction')
                printf("Check existence projections %g and %g\n",index_0,index_180)
            endif
        endif # creation par-files
    endif
    if ( recursive && shift_iterative_save_indiv_mat )
        save(sprintf('rhapp_iter_%d_%d.mat', projections(1), projections(end)),'rhapp_iter')
    endif
    if do_total_mean
        ht_total_mean_end
    endif
endif # shift
