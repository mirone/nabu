function [] = ht_mCTXsmall_overview_higher_100nm(varargin)

switch nargin
    case 0
        projections = [];
    case 1
        switch length(varargin{1})
            case 0
                projections = [];
            case 2
                projections = varargin{1}(1):varargin{1}(2);
            otherwise
                projections = varargin{1};
        endswitch
endswitch

projections = [ 1 ]
	
#######################
##### info on scan ####
#######################

#####################
# filenames
nvue= 1200; #!
refon= 1200; #!
#dir= "/tmp_14_days/mirone/id16/"; # with / at the end
dir= "/data/WORKS/"; # with / at the end
namestr= "mCTXsmall_overview_higher_100nm"; 
# namestr= "results"; 
numbers= [1 ]; 
vers=''; # will be added to name of phase maps to distinguish different versions

#################
# load h5 file and read some stuff
disp(fullfile(dir, sprintf("%s_%d_", namestr, numbers(1))) ) 
metadata = icat_h5_load(fullfile(dir, sprintf("%s_%d_", namestr, numbers(1))));
printf(["CALL icat_h5_metadata energy" ])
energy = icat_h5_metadata(metadata, "energy");
printf(["CALL icat_h5_metadata focus_position" ])
focus_position = icat_h5_metadata(metadata, "focus_position")

#################
# distances
sx0h = focus_position;
sx0v = focus_position;
printf(["CALL ht_get_motorpos" ])

sx = ht_get_motorpos(dir,namestr,numbers,"sx"); # read distances on the fly
z1_2 = 1.2648; # total distance focus- detector
z1h = (sx-sx0h)/1000; # distance source-sample
z1v = (sx-sx0v)/1000; # distance source-sample
check_z1v = 0; # determine z1v # 1 : checks vertical direction (case ID19) ; # 2 checks both directions
magnification = 1; # consider finite distance of source (spherical wave)
z2h = z1_2 - z1h; #!
z2v = z1_2 - z1v; #!
magnification_large = 1; # 0 parallel 1 projection -1 for finding pos_feature
correct_shrink = 0;

#################
# wavelength
lambda = 12.4e-10/energy; #!
printf("Energy is %g keV ; Wavelength is %g Ang\n", energy, lambda*1e10)

#################
# pixelsize
pixelsize_detector = 3e-06; #!

#################
# optics
optic_used = "IMG2_34keV"; # see mncf #!
oversamp = 1; #!
correct_detector = 3; # correction for detector or not
correct_ccd = ""; 
printf(["CALL header2correctccdpar\n" ])

correct_ccd_par = header2correctccdpar(header(sprintf('%s/%s_1_/%s_1_%4.4i.edf',dir,namestr,namestr,0)));
correct_distortion = ""; 
detector_distortion_prefix = '/data/id16a/inhouse1/instrument/img1/optique_peter_distortion/detector_distortion2d_';
for k = 1:length(numbers)
	  printf(["CALL SETTING h v cor par "       ])
	correct_distortion_par{k}.filenameh = sprintf('%sh.edf', detector_distortion_prefix);
	correct_distortion_par{k}.filenamev = sprintf('%sv.edf', detector_distortion_prefix);
printf(["CALL SETTING h v cor par "    correct_distortion_par{k}.filenameh  "\n"   ])
    #correct_distortion_par{k}.filename = sprintf('%s/talbotgridedf_/grid_distortion_%d_.mat', dir, numbers(k));
endfor
remove_spikes = 0.04;
correct_flat = 'distorted_flat'

#################################
##### info on reconstruction ####
#################################

# number of pixels (in reconstructed image)
n = 2048; # vertical, row, y
m = 2048; # horizontal, column, x
vb = 1; # first pixel in vertical direction
hb = 1; # first pixel in horizontal direction
pad_images = 1; # pad images to nearest power of two or give sizes

###################
####  shifts ######
###################

shift = 0; # set to 1 to determine the shifts, set to 0 if they are determined and stored in rhapp.mat
reference_quali = 1; # set to 1: does quali90 for all scans to see which moves less
#reference_quali_debug = 2; # 0: don't show images, 1: show before correction, 2: show after correction
reference_plane = 1; # plane with respect to which other planes will be aligned
reference_moved = 1; # take into account motion of reference plane or not
centralpart = 0;
#shift_approach = 2; # 0: DFT; 2: real space (default); 3: manual; 4: normxcorr2; 5: TurboReg in ImageJ
shift_estimate = 'fft';
#shift_distrib = 1; 
random_disp = 1; 
	
if (shift == 1)
    correct_whitefield = 'gaussian_filter';
    correct_whitefield_par = 14; 
else
    correct_whitefield = []; 
endif
    
###################
##### approach ####
###################

# only correction of shifts
# 0 no phase reconstruction
# only phase
# 1 pamsin
# phase + absorption
# 2 pamsin with absorption (absorption is slow envelope) (division by absorption)
# 3 pamcossin (absorption is weak)
# 4 mixture tie / pamsin
# 5 TIE with absorption
# 8 Paganin

approach = 1
delta_beta = 27 # used to estimate the low frequency part of phase if different from 0
#delta_beta = [0:500:2000]; # try to find optimum delta_beta
var_cut = 1;
#paganin_multiple_distance = 1; # 0 or 1 ; only for approach 8
recursive = 0;
#nloopm = 10; # number of iteration for recursive (default is 10)
recursive_purephase = 5; # keep amplitude Paganin
recursive_attenuate = 10;
recursive_constrain_grad = 0.6;
#post_filter = 'sinogram_filter' # put FBP filter in phase retrieval for better padding

###############################
# limits for 'wiener' filtering

#lim1 = 10.^[-8:-2]; # low frequencies
lim1 = 10^-5 # low frequencies
#lim2 = 10.^[-5:0.5:-0.5] # high frequencies
lim2 = 0.2
#lim2 = 0.2; # might be preferable for low resolution scan
#Lcurve = 1;

printf(["CALL holotomo_slave\n" ])


holotomo_slave

## for shifts:
#ht_shift_distrib_merge
