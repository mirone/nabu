## Copyright (C) 2008 P. Cloetens
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

## correct_motion_calc
## function [ref_h, ref_v] = correct_motion_calc(nvue,is360,dirname,correct_higher_order,reference_method,random_disp);
##      calculates horizontal and vertical correction to compensate for sample drift
##      uses information in quali.mat file stored by quali_series
##      creates a correct.txt file usable by PyHST
##      outputs corrections ref_h, ref_v
##      
##      arguments:
##      argument 1 : number of projections
##      argument 2 : scan over 360 or 180 degrees
##      argument 3 (optional): name of directory for quali.mat and correct.txt files (default : pwd)
##      argument 4 (optional): use maximum order for fits (1) or linear fits (0) ( default 1 )
##      argument 5 (optional): reference is begin scan (0) end scan (1) mean x/y components drift (2) 
##                              or subtract sinusoidal fit (3) ( default 3 )
##      argument 6 (optional): consider (1) or not (0) correct.txt file already exists due to random dispacement ( default 0 )
##                              if (1) save motion for pyhst in correct_motion.txt
##
##      see also: correct_motion, cmseries, holotomo_slave 

## Author: P. Cloetens <cloetens@esrf.fr>
##
## 2008-01-21 P. Cloetens <cloetens@esrf.fr>
## * Initial revision
## 2010-12-17 PC
## * Avoid error where polyfit_order_h could be non integer
## 2010-12-18 PC
## * Set maximum of 3 for the order of polynomial fits
## * Add drawnow after plotting
## 2011-01-10 PC
## * Correct plot to compare with experimental data and not fit itself
## 2011-01-10 PC
## * Correct plot to compare with experimental data and not fit itself
## 2011-08-29 PC
## * Avoid singular matrix M when reference position of scan would be used in fit
## 2015-09-24 MH
## * Introduce extra parameter random_disp to avoid overwriting original correct.txt file
## 2016-10-10 PC
## * Avoid continuation marker \
## 2020-11-01 PC
## * adapt to slurm for display; use isinteractive

function [ref_h, ref_v]=correct_motion_calc(nvue,is360,dirname,correct_higher_order,reference_method, random_disp);

    global FT_DEBUG

    if isempty(FT_DEBUG)
        debug = 0;
    else
        debug = FT_DEBUG;
    end

    # catch not given variables
    if !exist('dirname','var')
        dirname = '';
    endif
    if !exist('correct_higher_order','var')
        correct_higher_order = [];
    endif
    if !exist('reference_method','var')
        reference_method = [];
    endif
    if !exist('random_disp','var')
        random_disp = 0;
    endif
    
    # set defaults for empty variables
    if isempty(dirname)
        dirname = pwd;
    endif
    if isempty(correct_higher_order)
        correct_higher_order = 1;
    endif
    if isempty(reference_method)
        reference_method = 3;
    endif

    switch reference_method
        case 0 # reference is begin scan
            start_reference = 1;
            subtract_mean = 0;
            subtract_fit = 0;
        case 1 # reference is end scan
            start_reference = 0;
            subtract_mean = 0;
            subtract_fit = 0;
        case 2 # mean of x/y components of drift 0
            start_reference = 0;
            subtract_mean = 1;
            subtract_fit = 0;
        case 3 # subtract sinusoidal fit
            start_reference = 0;
            subtract_mean = 0;
            subtract_fit = 1;
    endswitch
    fname = [dirname '/quali.mat'];
    
    if !exist(fname,'file')
        printf('%s does not exist\n',fname)
        do_quali = 1;
    else
        try
            load(fname)
            do_quali = 0;
        catch
            printf('%s is obsolete\n',fname)
            do_quali = 1;
        end_try_catch
    endif
    if do_quali
        disp('We run quali_series now')
        quali_series(dirname);
        load(fname)
    endif

    # check if new version of quali_series was used
    if !exist("corr_imagesafterscan","var")
        if is360
            disp("quali.mat is obsolete, rerun quali_series")
            return
        endif
        disp("Updating variables")
        corr_imagesafterscan = [corrtot ; corrtot90];
        rot_positions = [0 90];
        rot_positions_index = [nvue+2 nvue+1];
    endif
    
    if is360
        scanrange = 360;
    else
        scanrange = 180; 
    endif
    all_proj = 0:nvue+length(rot_positions);
    rot_positions_usable_index = find(rot_positions != scanrange*(1-start_reference));
    if correct_higher_order
        polyfit_order_v = min(length(rot_positions_usable_index), 3);
        polyfit_order_h = min(floor(length(rot_positions_usable_index)/2), 3);
    else
        polyfit_order_v = 1;
        polyfit_order_h = 1;
    endif
    # arg should be 0 at 0 degrees if start is reference position
    # arg should be 0 at scanrange if end is reference position
    arg = rot_positions(rot_positions_usable_index)/scanrange - (1-start_reference);
    arg_all = all_proj/nvue - (1-start_reference); 
    # vertical
    M = start_reference - powerfit(arg,polyfit_order_v);
    p = corr_imagesafterscan(rot_positions_usable_index, 2)'/M;
    ref_v = polyval([p 0],arg_all);
	# horizontal
    M = start_reference - powerfit(arg,polyfit_order_h);
    M = [	M.*repmat(cos((pi/180)*rot_positions(rot_positions_usable_index)),polyfit_order_h,1) ;
    		M.*repmat(sin((pi/180)*rot_positions(rot_positions_usable_index)),polyfit_order_h,1)];
    p = corr_imagesafterscan(rot_positions_usable_index, 1)'/M;
    sx_cal = polyval([p(1:polyfit_order_h) 0],arg_all);
    sy_cal = polyval([p(polyfit_order_h+1:end) 0],arg_all);
    # subtract mean
    # always in vertical direction
    ref_v_mean = mean(ref_v);
    ref_v -= ref_v_mean;
    ref_v_meas = -corr_imagesafterscan(:,2)' - ref_v_mean;
    # flag in vertical direction
    if subtract_mean
        sx_mean = mean(sx_cal)
        sy_mean = mean(sy_cal)
        sx_cal -= sx_mean;
        sy_cal -= sy_mean;
    endif
    # prepare angles corresponding to all_proj (careful with images after scan)
    arg = all_proj*(scanrange/180*pi/nvue);
    arg(1+rot_positions_index) = rot_positions*(pi/180); 
    M = [ cos(arg) ; sin(arg) ];
    ref_h = sx_cal.*M(1,:) + sy_cal.*M(2,:);
    ref_h_meas = -corr_imagesafterscan(:,1)';
    index_rots = 1+round(rot_positions*(nvue/scanrange));
    if subtract_mean
        ref_h_meas -= sx_mean.*M(1, index_rots) + sy_mean.*M(2, index_rots);
    endif
    if subtract_fit
        disp("Fit with sinusoidal curve")
        p = ref_h/M;
        sx_cal -= p(1); # we update sx_cal and sy_cal for display
        sy_cal -= p(2);
        ref_h -= p*M;
        ref_h_meas -= p*M(:, index_rots);
    endif
	# display results
    if isinteractive
        figure(1);
        plot(all_proj, ref_h, 'r', all_proj, ref_v, 'b',
            all_proj(index_rots), ref_h_meas, 'r+', all_proj(index_rots), ref_v_meas, 'b+')
        legend('Horizontal','Vertical')
        title('Correction in pixels')
        drawnow
        if debug
            figure(2);
            plot(sx_cal(1:25:end),sy_cal(1:25:end),'+',
                sx_cal(index_rots),sy_cal(index_rots),'k+')
            title('Drift trajectory with respect to rotating frame')
            axis("equal")
            drawnow
            figure(3);
            axis("normal")
            subplot(2,1,1)
            plot(rot_positions,
                ref_h(1+rot_positions_index)-ref_h(index_rots),'ro',
                rot_positions,corr_imagesafterscan(:,1)','bx')
            title('Horizontal Correction')
            legend('fit','measure')
            drawnow
            subplot(2,1,2)
            plot(rot_positions,
                ref_v(1+rot_positions_index)-ref_v(index_rots),'ro',
                rot_positions,corr_imagesafterscan(:,2)','bx')
            title('Vertical Correction')
            drawnow
        endif
    endif
    # store correct.txt file for PyHST
    if ( random_disp )
		filename = [dirname '/correct.txt'];
                printf([ " APRO "  filename]); 
                printf(" giusto? \n");
		rand_disp = load(filename)';
		ref_h_save = ref_h;
		ref_v_save = ref_v;
		ref_h_save(1:size(rand_disp,2)) -= rand_disp(1,:);
		ref_v_save(1:size(rand_disp,2)) -= rand_disp(2,:);
                printf([" APRO ",  dirname]); 
                printf(" giusto? \n");
		fid = fopen([dirname '/correct_motion.txt'],'w');
		fprintf(fid,'%f %f\n',[-ref_h_save;-ref_v_save]);
		fclose(fid);
    else
		fid = fopen([dirname '/correct.txt'],'w');
		fprintf(fid,'%f %f\n',[-ref_h;-ref_v]);
		fclose(fid);
	endif
endfunction



