# Phase Retrieval

Phase Retrieval is the process of extracting the phase shift, induced by the sample on the beam, acquired on the detector. This phase shift is then used to reconstruct a map of the (deviation from unity of the real part of) refractive index through tomographic reconstruction.

Nabu implements the following non-iterative phase retrieval methods.

## Paganin phase retrieval

This phase retrieval method was first described in [[1]](#references).

The implementation [PaganinPhaseRetrieval](apidoc/nabu.preproc.phase.html#nabu.preproc.phase.PaganinPhaseRetrieval) follows the normalization of [[2]](#references).


The nabu implementation of Paganin phase retrieval should be compatible with the one of [tomopy](https://github.com/tomopy/tomopy). More precisely, given a `delta_beta` value, the result should be the same as the one provided by tomopy with

```python
extracted_phase_tomopy = retrieve_phase(
    data,
    pixel_size=pixel_size_micron*1e-4,
    dist=distance_cm,
    energy=energy_kev,
    alpha=1./(4*3.141592**2 * delta_beta),
)
```
there is notably a factor of `1/(4*pi**2)` between the conventions.

## Single-Distance CTF phase retrieval

This method, although based on Contrast Transfer Function (CTF) rather than Transport of Intensity (TIE), can also be expressed as a single step "filtering" processing. See for example [[2]](#references).

``` warning:: This method is not implemented for now.
```

## Unsharp masking

Single-distance phase retrieval tend to blur the images. To recover sharp features, a common method is to use a [unsharp mask](https://en.wikipedia.org/wiki/Unsharp_masking) image processing step.

This feature is implemented in [nabu.misc.unsharp](apidoc/nabu.misc.unsharp.html#module-nabu.misc.unsharp), and in the Cuda/Opencl back-ends [nabu.preproc.unsharp_cuda](apidoc/nabu.misc.unsharp_cuda.html#module-nabu.misc.unsharp_cuda) and [nabu.preproc.unsharp_opencl](apidoc/nabu.misc.unsharp_opencl.html#module-nabu.misc.unsharp_opencl) respectively.


## Enable phase retrieval from the configuration file

To enable the phase retrieval processing step, modify the section `[phase]` of [nabu config file](nabu_config_file.md):
```ini
[phase]
method = Paganin # possible values: none, Paganin, CTF
```
additional parameters can be specified:
```ini
# delta/beta ratio for the Paganin method
paganin_delta_beta = 100.0
# Padding type for the filtering step. Available are: mirror, edges, zeros
paganin_padding_type = mirror
```

To enable [unsharp masking](#unsharp-masking), the value of `unsharp_coeff` must be greater than zero.

```ini
# Unsharp mask strength. The unsharped image is equal to
#   UnsharpedImage =  (1 + coeff)*originalPaganinImage - coeff * ConvolutedImage. Setting this coefficient to zero means that no unsharp mask will be applied.
unsharp_coeff = 0
# Standard deviation of the Gaussian filter when applying an unsharp mask
# after the Paganin filtering. Disabled if set to 0.
unsharp_sigma = 0
```


## References

[1] D. Paganin Et Al, "Simultaneous phase and amplitude extraction from a single defocused image of a homogeneous object", Journal of Microscopy, Vol 206, Part 1, 2002

[2] Boliang Yu Et Al, "Evaluation of phase retrieval approaches in magnified X-ray phase nano computerized tomography applied to bone tissue," Opt. Express  26, 11110-11124 (2018)