# Installation

To install the current release:
```
pip install nabu
```

To install the current development version
```bash
pip install git+https://gitlab.esrf.fr/tomotools/nabu.git
```

## Dependencies

The above `pip install`  command should automatically install the nabu dependencies.

There are optional dependencides enabling various features:
  - Computations acceleration (GPU/manycore CPU): `pycuda`, `pyopencl`
  - Computations distribution: `distributed`, `dask_jobqueue`

Please note that Nabu supports Python >= 3.5.
