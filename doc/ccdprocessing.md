# CCD Processing

This section explains the various processing taking place in the "radios" domain.

## Introduction

Once a radios chunk is loaded into memory (see [radios chunk processing](definitions.md#radios-chunks)), various processing can be applied to each individual (sub)radio. The most common are:
  - CCD hotspots removal
  - Flat-Field normalization
  - Phase retrieval

## The `CCDProcessing` class

The base class for handling these processing is `CCDProcessing` found in `nabu.preproc.ccd`.  To instantiate a `CCDProcessing` class, only a stack of radios is required. 

![CCD processing classes](images/ccd_processing_class.png)



