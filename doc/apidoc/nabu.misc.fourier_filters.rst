nabu.misc.fourier\_filters module
=================================

.. automodule:: nabu.misc.fourier_filters
   :members:
   :undoc-members:
   :show-inheritance:
