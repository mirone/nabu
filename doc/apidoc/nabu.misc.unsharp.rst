nabu.misc.unsharp module
========================

.. automodule:: nabu.misc.unsharp
   :members:
   :undoc-members:
   :show-inheritance:
