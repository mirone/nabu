nabu.reconstruction.fbp\_opencl module
======================================

.. automodule:: nabu.reconstruction.fbp_opencl
   :members:
   :undoc-members:
   :show-inheritance:
