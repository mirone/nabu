nabu.opencl.utils module
========================

.. automodule:: nabu.opencl.utils
   :members:
   :undoc-members:
   :show-inheritance:
