nabu.misc.unsharp\_opencl module
================================

.. automodule:: nabu.misc.unsharp_opencl
   :members:
   :undoc-members:
   :show-inheritance:
