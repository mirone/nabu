.. Nabu documentation master file, created by
   sphinx-quickstart on Thu Oct  3 19:50:27 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Nabu |version|
==============

Nabu is a tomography software used at the `European Synchrotron <https://www.esrf.eu>`_.


Quick links:

   * `Nabu development place <https://gitlab.esrf.fr/tomotools/nabu/>`_
   * Project maintainer: `Pierre Paleo <mailto:pierre.paleo@esrf.fr>`_

Latest news:

  * December 2020: `Version 2020.5.0 released <v2020_5_0.md>`_
  * October 2020: `Version 2020.4.0 released <v2020_4_0.md>`_
  * August 2020: `Version 2020.3.0 released <v2020_3_0.md>`_
  * June 2020: `Version 2020.2.0 released <v2020_2_0.md>`_


Getting started
---------------

.. toctree::
   :maxdepth: 1
 
   install.md
   quickstart.md
   nabu_cli.md
   definitions.md
   nabu_config_file.md
   about.md


Features
--------
.. toctree::
   :maxdepth: 1

   features.md
   ccdprocessing.md
   phase.md   
   distribution.md



Tutorials
---------

.. toctree::
   :maxdepth: 2

   tutorials.rst


Advanced documentation
-----------------------

.. toctree::
   :maxdepth: 1

   tests.md
   pipeline.md
   nabu_tasks.md
   validators.md


API reference
--------------

.. toctree::
   :maxdepth: 3

   apidoc/nabu.rst


Release history
----------------

.. toctree::
   :maxdepth: 3


   v2020_5_0.md
   v2020_4_0.md
   v2020_3_0.md
   v2020_2_0.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
