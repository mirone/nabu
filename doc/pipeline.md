# The Nabu processing pipeline

This page explains how to use `nabu` as a tomography processing pipeline.

## 1. Introduction

Nabu is a library for tomography processing. As such, it can be used in various ways:

1. Calling the individual components of the library via the [Python API](apidoc/nabu). This is typically done when there are several specific tasks to perform, or  when you are building a program for a specific purpose
2. Define a processing pipeline through [Nabu internal processing steps representation](nabu_tasks.md). This is not recommended as many checks will not be done.	
3. Define a processing pipeline through the [configuration file](nabu_config_file.md) or the [ProcessConfig class](apidoc/nabu.resources.processconfig).

This page covers the points (2) and (3).

## 2. From configuration file to internal representation

This section explains how `nabu` digests a configuration file to build its final internal representation of processing steps.

A configuration file is first created by the user - either manually, or through a tool like `tomwer` or `nabu-config`. 

From this configuration file, a [ProcessConfig object](apidoc/nabu.resources.processconfig) is built. This object has two main fields: 

- `nabu_config` : the Python dictionary equivalent of the configuration file content, after some [validation steps](validators.md).
- `dataset_infos` : the result of the analysis of the dataset given in the configuration file. This structure gives information on how to access the data, along with some metadata (energy, distance, ...). Building this structure also entailed some checks (like removing unused radios).

The `ProcessConfig` object contains all the necessary information to perform the tomography processing steps. However, it is not structured in a way that can be directly used to distribute computations. This object is therefore converted to an internal representation describing the processing steps in a more formal representation. 

This conversion is done by the [build_processing_steps()](apidoc/nabu.resources.tasks.rst#nabu.resources.tasks.build_processing_steps) function, resulting in two structures described in [processing steps representation](nabu_tasks.md). These structures are the final internal representation of Nabu processing pipeline. 

## 3. Processing steps

In the current version, nabu defines the following processing steps:

- Read the data
- Flat-field normalization
- Radios-based rings artefacts correction
- CCD corrections
- Phase retrieval
- Sinogram-based rings artefacts correction
- Reconstruction

Each step can be enabled/disabled, but the order cannot be changed.

