## About

Short description : release version and purpose.

## To do
  - [ ] Feature 1
  - [ ] Feature 2
  - [ ] Ensure unit tests are passing
  - [ ] Perform reconstruction tests on various datasets/machines
  - [ ] Update API doc
  - [ ] Update doc: changelog, whatsnew, features, ...
  - [ ] Upload documentation
  - [ ] Deploy on pypi
  - [ ] Tag the branch

## Notes
Additional notes